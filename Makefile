
export PYTHONPATH = Source:Test

-include Makefile.local

test : check
	@Test/runtests.py $(RUNTESTS_FLAGS) -- $(RUNTESTS_TESTS)

profile : RUNTESTS_FLAGS += --profile
profile : test

benchmark : RUNTESTS_FLAGS += --benchmark
benchmark : test

time-only-benchmark : RUNTESTS_FLAGS += --benchmark --time-only
time-only-benchmark : test

detailed-benchmark : RUNTESTS_FLAGS += --benchmark --detailed-measurements
detailed-benchmark : test

tabulated-benchmark : RUNTESTS_FLAGS += --benchmark --tabulate-objects
tabulated-benchmark : test

detailed-tabulated-benchmark : RUNTESTS_FLAGS += --benchmark --detailed-measurements --tabulate-objects
detailed-tabulated-benchmark : test

resummarize : RUNTESTS_FLAGS += --resummarize
resummarize : test

verbose-% : RUNTESTS_FLAGS += --verbose
verbose-% : % ;

debug-% : RUNTESTS_FLAGS += --debug
debug-% : % ;

check : check-trailing-whitespace check-hg

check-trailing-whitespace :
	@echo "Check trailing whitespace ..."
	-! grep -Irn $(addprefix --exclude=,'*.rej' '*.svg' '*.json' '*.gz') '[         ]\+$$' Source Test Makefile* *.txt *.md

check-hg :
	hg id -nibtB 2>/dev/null || true
	hg qselect 2>/dev/null || true

clean :
	find . -name '*.py[oc]' -print -delete

repl :
	@command -v ipython > /dev/null && ipython || python

benchmark-listops : check
	python -m BenchmarkAdapton.BenchmarkListOps $(BENCHMARK_LISTOPS_FLAGS)

resummarize-listops : BENCHMARK_LISTOPS_FLAGS += --resummarize
resummarize-listops : benchmark-listops

visualize-listops : check
	python -m VisualizeAdapton.VisualizeListOps $(VISUALIZE_LISTOPS_FLAGS)
