from Utilities.Trampoline import *

class Eager(object):
    __slots__ = ( "value", )
    def __init__(self):
        raise NotImplementedError("call %s.make to construct %s" % ( self.__class__.__name__, self.__class__.__name__ ))
    def force(self):
        return self.value
    def yforce(self):
        return Ycall(self.force)
    def ytailforce(self):
        return Ytailcall(self.force)
    def __repr__(self):
        return "Eager(%r)" % ( self.value, )
    @Trampolining
    @classmethod
    def make(cls, fn, *args, **kwargs):
        self = cls.__new__(cls)
        if callable(fn):
            self.value = yield Ycall(fn, *args, **kwargs)
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self.value = fn
        yield self
