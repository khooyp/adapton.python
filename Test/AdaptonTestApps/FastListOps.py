from Utilities.Trampoline import *
from Utilities.Lazy import Lazy
from Adapton.Lazy.FastBidirectional import Lazy as FastBidirectionalLazyMod

__all__ = ( "FastBidirectionalLazyModListOps", )


class ListOps(object):
    def __init__(self, mod, name):
        if hasattr(mod, "make"):
            self.mod = mod.make
        else:
            self.mod = mod
        self.self_adjusting = hasattr(mod, "update")
        self.name = name

    def nil(self):
        return None

    def cons(self, head, tail):
        return ( head, tail )

    def of_list(self, xs):
        return reduce(lambda acc, x: self.mod(self.cons, x, acc), reversed(xs), self.mod(self.nil))

    def to_list(self, modlist):
        xs = []
        if hasattr(modlist, "refresh"):
            modlist.refresh()
        ys = modlist.force()
        while ys is not None:
            xs.append(ys[0])
            ys = ys[1].force()
        return xs

    def take(self, modlist, k):
        xs = []
        if hasattr(modlist, "refresh"):
            modlist.refresh()
        ys = modlist.force()
        n = 0
        while n < k:
            if ys is None:
                raise IndexError("take index out of range (%d of %d)" % ( k, n ))
            xs.append(ys[0])
            ys = ys[1].force()
            n += 1
        return xs

    def insert(self, xs, k, v):
        if k < 0:
            raise IndexError("insert index must be non-negative" % ( k, ))
        if self.self_adjusting:
            if k == 0:
                ys = xs.force()
                if ys is None:
                    xs.update(self.cons, v, self.mod(self.nil))
                else:
                    xs.update(self.cons, v, self.mod(self.cons, ys[0], ys[1]))
                return xs
            zs = xs
            n = 0
            while n < k:
                ys = zs
                zs = zs.force()
                if zs is None:
                    raise IndexError("insert index out of range (%d of %d)" % ( k, n ))
                y = zs[0]
                zs = zs[1]
                n += 1
            ys.update(self.cons, y, self.mod(self.cons, v, zs))
            return xs
        else:
            xs = self.to_list(xs)
            if len(xs) < k:
                raise IndexError("insert index out of range (%d of %d)" % ( k, len(xs) ))
            xs.insert(k, v)
            return self.of_list(xs)

    def delete(self, xs, k):
        if k < 0:
            raise IndexError("delete index must be non-negative" % ( k, ))
        if self.self_adjusting:
            if k == 0:
                ys = xs.force()
                value = ys[0]
                if ys is None:
                    raise IndexError("delete index out of range (0 of 0)")
                ys = ys[1].force()
                if ys is None:
                    xs.update(None)
                else:
                    xs.update(self.cons, ys[0], ys[1])
                return ( value, xs )
            zs = xs
            n = 0
            while n < k:
                ys = zs
                zs = ys.force()
                if zs is None:
                    raise IndexError("delete index out of range (%d of %d)" % ( k, n ))
                y = zs[0]
                zs = zs[1]
                n += 1
            zs = zs.force()
            if zs is None:
                raise IndexError("delete index out of range (%d of %d)" % ( k, n ))
            value = zs[0]
            ys.update(self.cons, y, zs[1])
            return ( value, xs )
        else:
            xs = self.to_list(xs)
            try:
                value = xs.pop(k)
            except IndexError:
                raise IndexError("delete index out of range (%d of %d)" % ( k, len(xs) ))
            return ( value, self.of_list(xs) )

    def concat(self, modlist1, modlist2):
        def concat_helper(modlist1, modlist2):
            llist = modlist1.force()
            if llist is None:
                return modlist2.force()
            else:
                head, tail = llist
                return self.cons(head, self.mod(concat_helper, tail, modlist2))
        return self.mod(concat_helper, modlist1, modlist2)

    def filter(self, fn, modlist):
        def filter_helper(fn, modlist):
            llist = modlist.force()
            if llist is None:
                return self.nil()
            else:
                head, tail = llist
                if fn(head):
                    return self.cons(head, self.mod(filter_helper, fn, tail))
                else:
                    return filter_helper(fn, tail)
        return self.mod(filter_helper, fn, modlist)

    def map(self, fn, modlist):
        def map_helper(fn, modlist):
            llist = modlist.force()
            if llist is None:
                return self.nil()
            else:
                head, tail = llist
                return self.cons((fn(head)), self.mod(map_helper, fn, tail))
        return self.mod(map_helper, fn, modlist)

    def scan(self, fn, modlist, acc):
        def scan_helper(fn, modlist, acc):
            llist = modlist.force()
            if llist is None:
                return self.nil()
            else:
                head, tail = llist
                acc = fn(head, acc)
                return self.cons(acc, self.mod(scan_helper, fn, tail, acc))
        return self.mod(scan_helper, fn, modlist, acc)

    def split(self, modlistpair):
        left = self.mod(lambda modlistpair: modlistpair.force()[0].force(), modlistpair)
        right = self.mod(lambda modlistpair: modlistpair.force()[1].force(), modlistpair)
        return ( left, right )

    def partition(self, key, modlist):
        def partition_helper(key, modlist):
            llist = modlist.force()
            if llist is None:
                return ( self.mod(self.nil), self.mod(self.nil) )
            else:
                head, tail = llist
                left, right = self.split(self.mod(partition_helper, key, tail))
                if head < key:
                    return ( self.mod(self.cons, head, left), right )
                else:
                    return ( left, self.mod(self.cons, head, right) )
        return self.split(self.mod(partition_helper, key, modlist))

    def quicksort(self, modlist):
        def quicksort_helper(modlist, rest):
            llist = modlist.force()
            if llist is None:
                return rest.force()
            else:
                head, tail = llist
                left, right = self.partition(head, tail)
                right = self.mod(quicksort_helper, right, rest)
                return quicksort_helper(left, self.mod(self.cons, head, right))
        return self.mod(quicksort_helper, modlist, self.mod(self.nil))

    def __str__(self):
        return self.name


FastBidirectionalLazyModListOps = ListOps(FastBidirectionalLazyMod, "FastBidirectionalLazyMod")
