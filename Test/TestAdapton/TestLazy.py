import MyPyUnit
from MyPyUnit import QC
from itertools import chain, izip
from random import randrange
from Utilities.ListTools import tfold
from Utilities.Trampoline import *
from Adapton.Lazy import Naive, Debug, Bidirectional, FastBidirectional


def make_counter():
    def counter(fn, label):
        from functools import wraps
        @Trampolining
        @wraps(fn)
        def wrapper(*args, **kwargs):
            result = yield Ycall(fn, *args, **kwargs)
            counter.count += 1
            yield result
        wrapper.label = label
        return wrapper
    counter.count = 0
    return counter


@MyPyUnit.parameterized_test_module(
    Naive=Naive.Lazy, Debug=Debug.Lazy, Bidirectional=Bidirectional.Lazy, FastBidirectional=FastBidirectional.Lazy)
def make_list_ops_test_case(Lazy):
    global Correctness, Regression

    class Correctness(MyPyUnit.TestCase):
        @QC.forall(
            a1=QC.integers(), b1=QC.integers(), c1=QC.integers(),
            a2=QC.integers(), b2=QC.integers(), c2=QC.integers(),
            ks=QC.permutations(xrange(3)),
            os=QC.lists(size=(3, 3), items=QC.booleans()))
        @QC.include(a1=56, a2=54, b1=93, b2=11, c1=11, c2=68, ks=[ 2, 0, 1 ], os=[ False, False, False ])
        def test_a_b_plus_c_update_a_b_c(self, a1, b1, c1, a2, b2, c2, ks, os):
            counter = make_counter()
            aa = Lazy(counter(lambda: a1, "a1"))
            bb = Lazy(counter(lambda: b1, "b1"))
            cc = Lazy(counter(lambda: c1, "c1"))
            dd = Lazy(counter(lambda: aa.force() * bb.force(), "d"))
            ee = Lazy(counter(lambda: dd.force() + cc.force(), "e"))
            ff = Lazy(counter(lambda: bb.force() * cc.force(), "f"))
            gg = Lazy(counter(lambda: aa.force() + ff.force(), "g"))
            def check(order):
                if order:
                    self.assertEqual(ee.force(), a1 * b1 + c1)
                else:
                    self.assertEqual(gg.force(), a1 + b1 * c1)
            self.assertEqual(counter.count, 0)
            check(True)
            max_count = counter.count
            counter.count = 0
            check(False)
            self.assertLess(counter.count, max_count)
            self.assertGreater(counter.count, 0)

            for k, o in izip(ks, os):
                counter.count = 0
                if k == 0:
                    a1 = a2
                    aa.update(counter(lambda: a1, "a2"))
                elif k == 1:
                    b1 = b2
                    bb.update(counter(lambda: b1, "b2"))
                elif k == 2:
                    c1 = c2
                    cc.update(counter(lambda: c1, "c2"))
                self.assertEqual(counter.count, 0)
                check(o)
                self.assertLess(counter.count, max_count)
                counter.count = 0
                check(not o)
                self.assertLess(counter.count, max_count)

        @QC.forall(xs=QC.lists(size=( 3, 20 )), kss=QC.lists(size=( 5, 10 ), items=QC.dicts(size=( 1, 10 ))))
        @QC.where(lambda xs, kss: all( len(ks) < len(xs) for ks in kss ))
        @QC.include(xs=[ 1, 2, 3 ], kss=[ { 1: 4, 2: 5 } ])
        def test_tree_sum_update_inputs(self, xs, kss):
            counter = make_counter()
            xs = list(xs)
            zs = [ Lazy(x) for x in xs ]
            ys = [ Lazy(counter(lambda z: z.force(), str(k)), z) for k, z in enumerate(zs) ]
            ysum = tfold(lambda x, y: Lazy(counter(lambda: x.force() + y.force(),
                "%s.%s" % ( x.thunk.func.label if x.thunk else x.value, y.thunk.func.label if y.thunk else y.value ))), ys)
            self.assertEqual(counter.count, 0)
            self.assertEqual(ysum.force(), sum(xs))
            self.assertEqual(counter.count, 2 * len(xs) - 1)
            max_count = counter.count

            for ks in kss:
                counter.count = 0
                for k, v in ks.iteritems():
                    k = k % len(xs)
                    xs[k] = v
                    ys[k].update(v)
                self.assertEqual(counter.count, 0)
                self.assertEqual(ysum.force(), sum(xs))
                self.assertLess(counter.count, max_count)

        @QC.forall(xs=QC.lists(size=( 3, 20 )), kqss=QC.lists(size=( 5, 10 ), items=QC.tuples(QC.samples(xrange(3, 20)), QC.samples(xrange(3, 20)))))
        @QC.where(lambda xs, kqss: all( len(ks) < len(xs) and len(qs) < len(xs) for ks, qs in kqss ))
        def test_tree_sum_update_commute_node(self, xs, kqss):
            counter = make_counter()
            ys = [ ( Lazy(counter(lambda x: x, [ k ]), x), x)  for k, x in enumerate(xs) ]
            zs = []
            while len(ys) > 1:
                o = randrange(len(ys) - 1)
                z = Lazy(counter(lambda x, y: x.force() + y.force(), [ ys[o][0].thunk.func.label, ys[o + 1][0].thunk.func.label ]), ys[o][0], ys[o + 1][0])
                zs.append(( z, ys[o][0], ys[o + 1][0], ys[o][1] + ys[o + 1][1] ))
                ys[o:o + 2] = ( ( z, ys[o][1] + ys[o + 1][1] ), )
            ysum = ys[0][0]
            xsum = sum(xs)
            self.assertEqual(ys[0][1], xsum)
            del z, ys
            self.assertEqual(counter.count, 0)
            self.assertEqual(ysum.force(), xsum)
            self.assertEqual(counter.count, 2 * len(xs) - 1)

            count = 0
            for ks, qs in kqss:
                counter.count = 0
                kset = set()
                for k in ks:
                    k = k % len(zs)
                    kset.add(k)
                    z = zs[k]
                    z[0].update(counter(lambda x, y: x.force() + y.force(), [ z[2].thunk.func.label, z[1].thunk.func.label ]), z[2], z[1])
                    zs[k] = ( z[0], z[2], z[1], z[3] )
                count += len(kset)
                self.assertEqual(counter.count, 0)
                for q in qs:
                    counter.count = 0
                    q = q % len(zs)
                    self.assertEqual(zs[q][0].force(), zs[q][3])
                    self.assertLessEqual(counter.count, count)
                    count -= counter.count
            counter.count = 0
            self.assertEqual(ysum.force(), xsum)
            self.assertLessEqual(counter.count, count)

        @QC.forall(xs=QC.lists(size=( 3, 20 )), kqss=QC.lists(size=( 5, 10 ), items=QC.tuples(QC.samples(xrange(3, 20)), QC.samples(xrange(3, 20)))))
        @QC.where(lambda xs, kqss: all( len(ks) < len(xs) and len(qs) < len(xs) for ks, qs in kqss ))
        def test_tree_sum_update_commute_node_batched(self, xs, kqss):
            counter = make_counter()
            ys = [ ( Lazy(counter(lambda x: x, [ k ]), x), x)  for k, x in enumerate(xs) ]
            zs = []
            while len(ys) > 1:
                o = randrange(len(ys) - 1)
                z = Lazy(counter(lambda x, y: x.force() + y.force(), [ ys[o][0].thunk.func.label, ys[o + 1][0].thunk.func.label ]), ys[o][0], ys[o + 1][0])
                zs.append(( z, ys[o][0], ys[o + 1][0], ys[o][1] + ys[o + 1][1] ))
                ys[o:o + 2] = ( ( z, ys[o][1] + ys[o + 1][1] ), )
            ysum = ys[0][0]
            xsum = sum(xs)
            self.assertEqual(ys[0][1], xsum)
            del z, ys
            self.assertEqual(counter.count, 0)
            self.assertEqual(ysum.force(), xsum)
            self.assertEqual(counter.count, 2 * len(xs) - 1)

            for ks, qs in kqss:
                counter.count = 0
                kset = set()
                for k in ks:
                    k = k % len(zs)
                    kset.add(k)
                    z = zs[k]
                    z[0].update(counter(lambda x, y: x.force() + y.force(), [ z[2].thunk.func.label, z[1].thunk.func.label ]), z[2], z[1])
                    zs[k] = ( z[0], z[2], z[1], z[3] )
                self.assertEqual(counter.count, 0)
                count = 0
                for q in qs:
                    counter.count = 0
                    q = q % len(zs)
                    self.assertEqual(zs[q][0].force(), zs[q][3])
                    self.assertLessEqual(counter.count, len(kset))
                    count += counter.count
                counter.count = 0
                self.assertEqual(ysum.force(), xsum)
                self.assertEqual(counter.count + count, len(kset))

        @QC.forall(
            xs=QC.lists(size=( 3, 15 )),
            kqss=QC.lists(size=( 5, 10 ), items=QC.tuples(QC.lists(size=( 3, 15 ), items=QC.tuples(QC.integers(), QC.integers())), QC.samples(xrange(3, 15)))))
        @QC.where(lambda xs, kqss: all( len(ks) < len(xs) and len(qs) < len(xs) for ks, qs in kqss ))
        def test_tree_sum_update_commute_disjoint(self, xs, kqss):
            counter = make_counter()
            ys = [ Lazy(counter(lambda x: x, [ ( k, x ) ]), x) for k, x in enumerate(xs) ]
            zs = []
            while len(ys) > 1:
                o = randrange(len(ys) - 1)
                z = Lazy(counter(lambda x, y: x.force() + y.force(), [ ys[o].thunk.func.label, ys[o + 1].thunk.func.label ]), ys[o], ys[o + 1])
                zs.append(( z, ys[o], ys[o + 1] ))
                ys[o:o + 2] = ( z, )
            ysum = ys[0]
            del z, ys
            xsum = sum(xs)
            def flatten(label):
                return chain.from_iterable( flatten(x) if isinstance(x, list) else ( x[1], ) for x in label )
            self.assertEqual(sum(flatten(ysum.thunk.func.label)), xsum)
            self.assertEqual(counter.count, 0)
            self.assertEqual(ysum.force(), xsum)
            self.assertEqual(counter.count, 2 * len(xs) - 1)
            max_count = len(xs) - 1

            for ks, qs in kqss:
                counter.count = 0
                for k1, k2 in ks:
                    k1 = k1 % len(zs)
                    k2 = k2 % len(zs)
                    if k1 == k2:
                        z1 = zs[k1]
                        z1label = z1[0].thunk.func.label
                        z1label[:] = [ z1[2].thunk.func.label, z1[1].thunk.func.label ]
                        z1[0].update(counter(lambda x, y: x.force() + y.force(), z1label), z1[2], z1[1])
                        zs[k1] = ( z1[0], z1[2], z1[1] )
                    else:
                        z1 = zs[k1]
                        z2 = zs[k2]
                        z1label = z1[0].thunk.func.label
                        z2label = z2[0].thunk.func.label
                        if str(z2[1].thunk.func.label) not in str(z1[2].thunk.func.label) and str(z1[2].thunk.func.label) not in str(z2[1].thunk.func.label):
                            z1label[:] = [ z1[1].thunk.func.label, z2[1].thunk.func.label ]
                            z2label[:] = [ z1[2].thunk.func.label, z2[2].thunk.func.label ]
                            z1[0].update(counter(lambda x, y: x.force() + y.force(), z1label), z1[1], z2[1])
                            z2[0].update(counter(lambda x, y: x.force() + y.force(), z2label), z1[2], z2[2])
                            zs[k1] = ( z1[0], z1[1], z2[1] )
                            zs[k2] = ( z2[0], z1[2], z2[2] )
                        elif str(z2[2].thunk.func.label) not in str(z1[1].thunk.func.label) and str(z1[1].thunk.func.label) not in str(z2[2].thunk.func.label):
                            z1label[:] = [ z2[2].thunk.func.label, z1[2].thunk.func.label ]
                            z2label[:] = [ z2[1].thunk.func.label, z1[1].thunk.func.label ]
                            z1[0].update(counter(lambda x, y: x.force() + y.force(), z1label), z2[2], z1[2])
                            z2[0].update(counter(lambda x, y: x.force() + y.force(), z2label), z2[1], z1[1])
                            zs[k1] = ( z1[0], z2[2], z1[2] )
                            zs[k2] = ( z2[0], z2[1], z1[1] )
                        elif str(z2[2].thunk.func.label) not in str(z1[2].thunk.func.label) and str(z1[2].thunk.func.label) not in str(z2[2].thunk.func.label):
                            z1label[:] = [ z1[1].thunk.func.label, z2[2].thunk.func.label ]
                            z2label[:] = [ z2[1].thunk.func.label, z1[2].thunk.func.label ]
                            z1[0].update(counter(lambda x, y: x.force() + y.force(), z1label), z1[1], z2[2])
                            z2[0].update(counter(lambda x, y: x.force() + y.force(), z2label), z2[1], z1[2])
                            zs[k1] = ( z1[0], z1[1], z2[2] )
                            zs[k2] = ( z2[0], z2[1], z1[2] )
                        else:
                            z1label[:] = [ z2[1].thunk.func.label, z1[2].thunk.func.label ]
                            z2label[:] = [ z1[1].thunk.func.label, z2[2].thunk.func.label ]
                            z1[0].update(counter(lambda x, y: x.force() + y.force(), z1label), z2[1], z1[2])
                            z2[0].update(counter(lambda x, y: x.force() + y.force(), z2label), z1[1], z2[2])
                            zs[k1] = ( z1[0], z2[1], z1[2] )
                            zs[k2] = ( z2[0], z1[1], z2[2] )
                self.assertEqual(counter.count, 0)
                for q in qs:
                    counter.count = 0
                    q = q % len(zs)
                    self.assertEqual(zs[q][0].force(), sum(flatten(zs[q][0].thunk.func.label)))
                    self.assertLessEqual(counter.count, max_count)
            counter.count = 0
            self.assertEqual(ysum.force(), xsum)
            self.assertLessEqual(counter.count, max_count)


    class Regression(MyPyUnit.TestCase):
        def test_simple(self):
            x = Lazy(lambda: 1)
            y = Lazy(x.force)
            self.assertEqual(x.force(), 1)
            self.assertEqual(y.force(), 1)
            x.update(lambda: 2)
            self.assertEqual(x.force(), 2)
            self.assertEqual(y.force(), 2)

        def test_a_b_plus_c_update_a_b(self):
            counter = make_counter()
            aa = Lazy(counter(lambda: 1, "a1"))
            bb = Lazy(counter(lambda: 2, "b1"))
            cc = Lazy(counter(lambda: 3, "c1"))
            dd = Lazy(counter(lambda: aa.force() * bb.force(), "d"))
            ee = Lazy(counter(lambda: dd.force() + cc.force(), "e"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 1 * 2 + 3)
            self.assertEqual(counter.count, 5)

            counter.count = 0
            aa.update(counter(lambda: 1, "a2"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 1 * 2 + 3)
            self.assertEqual(counter.count, 1)

            counter.count = 0
            bb.update(counter(lambda: 5, "b2"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 1 * 5 + 3)
            self.assertEqual(counter.count, 3)

        def test_a_b_plus_c_update_b_a(self):
            counter = make_counter()
            aa = Lazy(counter(lambda: 1, "a1"))
            bb = Lazy(counter(lambda: 2, "b1"))
            cc = Lazy(counter(lambda: 3, "c1"))
            dd = Lazy(counter(lambda: aa.force() * bb.force(), "d"))
            ee = Lazy(counter(lambda: dd.force() + cc.force(), "e"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 1 * 2 + 3)
            self.assertEqual(counter.count, 5)

            counter.count = 0
            bb.update(counter(lambda: 2, "b2"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 1 * 2 + 3)
            self.assertEqual(counter.count, 1)

            counter.count = 0
            aa.update(counter(lambda: 5, "a2"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(ee.force(), 5 * 2 + 3)
            self.assertEqual(counter.count, 3)

        def test_tree_sum_1_2_3_4(self):
            counter = make_counter()
            a = Lazy(counter(lambda: 1, "a"))
            b = Lazy(counter(lambda: 2, "b"))
            c = Lazy(counter(lambda: 3, "c"))
            d = Lazy(counter(lambda: 4, "d"))
            e = Lazy(counter(lambda: a.force() + b.force(), "e"))
            f = Lazy(counter(lambda: c.force() + d.force(), "f"))
            g = Lazy(counter(lambda: e.force() + f.force(), "g"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(g.force(), sum(( 1, 2, 3, 4 )))
            self.assertEqual(counter.count, 7)

        def test_tree_sum_1_2_3_4_commute(self):
            counter = make_counter()
            a = Lazy(counter(lambda: 1, "a"))
            b = Lazy(counter(lambda: 2, "b"))
            c = Lazy(counter(lambda: 3, "c"))
            d = Lazy(counter(lambda: 4, "d"))
            e = Lazy(counter(lambda: a.force() + b.force(), "e1"))
            f = Lazy(counter(lambda: c.force() + d.force(), "f1"))
            g = Lazy(counter(lambda: e.force() + f.force(), "g1"))
            xsum = sum(( 1, 2, 3, 4 ))
            self.assertEqual(counter.count, 0)
            self.assertEqual(g.force(), xsum)
            self.assertEqual(counter.count, 7)

            counter.count = 0
            e.update(counter(lambda: b.force() + a.force(), "e2"))
            g.update(counter(lambda: f.force() + e.force(), "g2"))
            self.assertEqual(counter.count, 0)
            self.assertEqual(g.force(), xsum)
            self.assertEqual(counter.count, 2)
