import sys, time, gc, resource
from collections import OrderedDict, Counter, namedtuple

__all__ = (
    "DetailedTabulatedMeasurement", "DetailedMeasurement", "TabulatedMeasurement", "Measurement",
    "TimeOnlyMeasurement", "NullMeasurement", "MeasurementGroup"
)
__unittest = True


try:
    statm = open("/proc/self/statm")
except IOError:
    def getVmSize():
        return 0
else:
    import resource
    pagesize = resource.getpagesize()
    def getVmSize():
        statm.seek(0)
        statm.flush()
        line = statm.readline()
        return int(line[:line.index(" ")]) * pagesize


class MeasurementCounter(Counter):
    __slots__ = ()


class MeasurementOrderedDict(OrderedDict):
    __slots__ = ()


def getGcSize():
    # do not filter out MeasurementCounter/MeasurementOrderedDict, since type testing is surprisingly slow
    return sum( sys.getsizeof(obj, 0) for obj in gc.get_objects() )


def getGcStats():
    objects = gc.get_objects()
    size = 0
    table = MeasurementCounter()
    for obj in objects:
        objsize = sys.getsizeof(obj, 0)
        size += objsize
        table[type(obj)] += objsize
    size -= table.pop(MeasurementCounter, 0) + table.pop(MeasurementOrderedDict, 0)
    return ( size, table )


DescribedMeasurements = namedtuple("DescribedMeasurements", ( "description", "measurement" ))

measurement_stack = []

class DetailedTabulatedMeasurement(object):
    __slots__ = ( "test", "label", "extradata", "start_gc_size", "start_gc_table", "start_vm_size", "start_rusage", "start_time" )
    description = OrderedDict((
        ( "time", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "utime", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "stime", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "vm_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_table", OrderedDict(( ( "type", "tabular" ), ( "subtype", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_vm_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_gc_table", OrderedDict(( ( "type", "tabular" ), ( "subtype", "ratio" ), ( "unit", "bytes" ) )) ),
    ))

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        gc.collect()
        self.start_gc_size, self.start_gc_table = getGcStats()
        gc.collect()
        self.start_vm_size = getVmSize()
        gc.collect()
        self.start_rusage = resource.getrusage(resource.RUSAGE_SELF)
        self.start_time = time.time()

    def __exit__(self, exc, value, tb):
        if exc is None:
            stop_time = time.time()
            stop_rusage = resource.getrusage(resource.RUSAGE_SELF)
            stop_vm_size = getVmSize()
            stop_gc_size, stop_gc_table = getGcStats()
            stop_gc_table.subtract(self.start_gc_table)
            stop_gc_table = MeasurementOrderedDict(sorted(
                ( "%s.%s" % ( cls.__module__, cls.__name__ ), size ) for cls, size in stop_gc_table.iteritems() if size != 0 ))
            gc.collect()
            gc_vm_size = getVmSize()
            gc.collect()
            gc_gc_size, gc_gc_table = getGcStats()
            gc_gc_table.subtract(self.start_gc_table)
            gc_gc_table = MeasurementOrderedDict(sorted(
                ( "%s.%s" % ( cls.__module__, cls.__name__ ), size ) for cls, size in gc_gc_table.iteritems() if size != 0 ))
            # note that gc_size/gc_table/gc_gc_size/gc_gc_table have systematic bias due to the __enter__ and __exit__ call frames
            measurements = {
                "time": stop_time - self.start_time,
                "utime": stop_rusage.ru_utime - self.start_rusage.ru_utime,
                "stime": stop_rusage.ru_stime - self.start_rusage.ru_stime,
                "vm_size": stop_vm_size - self.start_vm_size,
                "gc_size": stop_gc_size - self.start_gc_size,
                "gc_table": stop_gc_table, # subtracted above
                "gc_vm_size": gc_vm_size - self.start_vm_size,
                "gc_gc_size": gc_gc_size - self.start_gc_size,
                "gc_gc_table": gc_gc_table, # subtracted above
            }
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(DescribedMeasurements(self.description, measurements))


class DetailedMeasurement(object):
    __slots__ = ( "test", "label", "extradata", "start_gc_size", "start_vm_size", "start_rusage", "start_time" )
    description = OrderedDict((
        ( "time", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "utime", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "stime", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "vm_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_vm_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
    ))

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        gc.collect()
        self.start_gc_size = getGcSize()
        gc.collect()
        self.start_vm_size = getVmSize()
        gc.collect()
        self.start_rusage = resource.getrusage(resource.RUSAGE_SELF)
        self.start_time = time.time()

    def __exit__(self, exc, value, tb):
        if exc is None:
            stop_time = time.time()
            stop_rusage = resource.getrusage(resource.RUSAGE_SELF)
            stop_vm_size = getVmSize()
            stop_gc_size = getGcSize()
            gc.collect()
            gc_vm_size = getVmSize()
            gc.collect()
            gc_gc_size = getGcSize()
            # note that gc_size/gc_gc_size have systematic bias due to the __enter__ and __exit__ call frames
            measurements = {
                "time": stop_time - self.start_time,
                "utime": stop_rusage.ru_utime - self.start_rusage.ru_utime,
                "stime": stop_rusage.ru_stime - self.start_rusage.ru_stime,
                "vm_size": stop_vm_size - self.start_vm_size,
                "gc_size": stop_gc_size - self.start_gc_size,
                "gc_vm_size": gc_vm_size - self.start_vm_size,
                "gc_gc_size": gc_gc_size - self.start_gc_size,
            }
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(DescribedMeasurements(self.description, measurements))


class TabulatedMeasurement(object):
    __slots__ = ( "test", "label", "extradata", "start_gc_size", "start_gc_table", "start_time" )
    description = OrderedDict((
        ( "time", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "gc_gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
        ( "gc_gc_table", OrderedDict(( ( "type", "tabular" ), ( "subtype", "ratio" ), ( "unit", "bytes" ) )) ),
    ))

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        gc.collect()
        self.start_gc_size, self.start_gc_table = getGcStats()
        gc.collect()
        self.start_time = time.time()

    def __exit__(self, exc, value, tb):
        if exc is None:
            stop_time = time.time()
            gc.collect()
            gc_gc_size, gc_gc_table = getGcStats()
            gc_gc_table.subtract(self.start_gc_table)
            gc_gc_table = MeasurementOrderedDict(sorted(
                ( "%s.%s" % ( cls.__module__, cls.__name__ ), size ) for cls, size in gc_gc_table.iteritems() if size != 0 ))
            # note that gc_gc_size/gc_gc_table have systematic bias due to the __enter__ and __exit__ call frames
            measurements = {
                "time": stop_time - self.start_time,
                "gc_gc_size": gc_gc_size - self.start_gc_size,
                "gc_gc_table": gc_gc_table, # subtracted above
            }
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(DescribedMeasurements(self.description, measurements))


class Measurement(object):
    __slots__ = ( "test", "label", "extradata", "start_gc_size", "start_time" )
    description = OrderedDict((
        ( "time", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
        ( "gc_gc_size", OrderedDict(( ( "type", "ratio" ), ( "unit", "bytes" ) )) ),
    ))

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        gc.collect()
        self.start_gc_size = getGcSize()
        gc.collect()
        self.start_time = time.time()

    def __exit__(self, exc, value, tb):
        if exc is None:
            stop_time = time.time()
            gc.collect()
            gc_gc_size = getGcSize()
            # note that gc_gc_size has systematic bias due to the __enter__ and __exit__ call frames
            measurements = {
                "time": stop_time - self.start_time,
                "gc_gc_size": gc_gc_size - self.start_gc_size,
            }
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(DescribedMeasurements(self.description, measurements))


class TimeOnlyMeasurement(object):
    __slots__ = ( "test", "label", "extradata", "start_time" )
    description = OrderedDict((
        ( "time", OrderedDict(( ( "type", "ratio" ), ( "unit", "seconds" ) )) ),
    ))

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        self.start_time = time.time()

    def __exit__(self, exc, value, tb):
        if exc is None:
            stop_time = time.time()
            measurements = {
                "time": stop_time - self.start_time,
            }
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(DescribedMeasurements(self.description, measurements))


class NullMeasurement(object):
    __slots__ = ()
    description = {}

    def __init__(self, test, label, extradata={}):
        pass

    def __enter__(self):
        pass

    def __exit__(self, exc, value, tb):
        pass


class MeasurementGroup(object):
    __slots__  = ( "test", "label", "extradata", "measurements" )

    def __init__(self, test, label, extradata={}):
        self.test = test
        self.label = label
        self.extradata = extradata

    def __enter__(self):
        self.measurements = []
        measurement_stack.append(self.measurements)

    def __exit__(self, exc, value, tb):
        assert measurement_stack.pop() is self.measurements
        if exc is None:
            measurements = {}
            for description, m in self.measurements:
                for measurement, value in m.iteritems():
                    if description[measurement]["type"] in ( "ordinal", "interval", "ratio" ):
                        measurements[measurement] = measurements.get(measurement, 0) + value
                    elif description[measurement]["type"] == "tabular" and description[measurement]["subtype"] in ( "ordinal", "interval", "ratio" ):
                        measurements.setdefault(measurement, Counter()).update(value)
                    else:
                        raise NotImplementedError("MeasurementGroup does not yet support measurement %s" % ( description[k], ))
            for measurement, value in measurements.iteritems():
                if description[measurement]["type"] == "tabular":
                    measurements[measurement] = OrderedDict(sorted( ( clsname, size ) for clsname, size in value.iteritems() if size != 0 ))
            record = {
                "label": self.label,
                "measurements": measurements
            }
            record.update(self.extradata)
            self.test.result.addBenchmarkRecord(self.test, record)
            if measurement_stack:
                measurement_stack[-1].append(measurements)

