import sys, os, shutil, time, traceback, unittest, re, gzip
from collections import OrderedDict, Counter, namedtuple
from .Measurements import *

__unittest = True


from unittest import TestSuite, expectedFailure, skip


testmodule_re = re.compile(r"(?:Test|Benchmark)\w+$")
testfn_re = re.compile(r"(?:test|benchmark)\w+$")


def getTestCaseNames(testCaseClass):
    return sorted( name for name in dir(testCaseClass) \
        if testfn_re.match(name) and name not in dir(Benchmark) and callable(getattr(testCaseClass, name)) )


class TestResult(unittest.TestResult):
    """
    Test results container that immediately logs errors and failures
    as they are encountered, as well as the run time of tests.
    """
    def __init__(self, stream, descriptions, verbosity, profile, benchmark, time_only, detailed_measurements, tabulate_objects,
            benchmark_notify_interval=300, benchmark_notify_percent=0.1):
        super(TestResult, self).__init__()
        self.stream = stream
        self.descriptions = descriptions
        self.verbosity = verbosity
        self.profile = profile
        if benchmark:
            self.benchmark = os.path.join(benchmark, time.strftime("%Y-%-m-%d-%H-%M-%S"))
        else:
            self.benchmark = None
        self.time_only = time_only
        self.detailed_measurements = detailed_measurements
        self.tabulate_objects = tabulate_objects
        self.benchmark_notify_interval = benchmark_notify_interval
        self.benchmark_notify_percent = benchmark_notify_percent
        self.benchmark_test = None
        self.benchmark_block = None

    def fail(self, level, test, err):
        self.fail_args = ( level, test, err )

    def startTestRun(self):
        print>>self.stream, "=" * 70
        super(TestResult, self).startTestRun()

    def stopTestRun(self):
        super(TestResult, self).stopTestRun()
        print>>self.stream, "-" * 70

    def startBenchmark(self, test):
        if not os.path.isdir(self.benchmark):
            if self.verbosity > 1:
                print>>self.stream, "Creating benchmark directory at %s ..." % ( self.benchmark, )
            os.makedirs(self.benchmark)
            parent, dir = os.path.split(self.benchmark)
            latest = os.path.join(parent, "latest")
            if self.verbosity > 1:
                print>>self.stream, "Updating latest symlink to benchmark ..."
            try:
                os.unlink(latest)
            except OSError:
                pass
            try:
                os.symlink(dir, latest)
            except OSError:
                print>>self.stream, traceback.format_exc()
                print>>self.stream, "warning: cannot create latest symlink to benchmark"
            sysinfo_dir = os.path.join(self.benchmark, "SystemInfo")
            try:
                os.mkdir(sysinfo_dir)
            except OSError:
                print>>self.stream, "warning: cannot create SystemInfo directory for %s" % ( self.benchmarkId(), )
            else:
                for file in ( "cpuinfo", "meminfo", "version", "loadavg" ):
                    path = os.path.join("/proc", file)
                    if self.verbosity > 1:
                        print>>self.stream, "Copying %s to benchmark ..." % ( path, )
                    try:
                        shutil.copyfile(path, os.path.join(sysinfo_dir, file))
                    except OSError:
                        print>>self.stream, traceback.format_exc()
                        print>>self.stream, "warning: cannot copy %s to benchmark" % ( path, )

        print>>self.stream, "%s ..." % ( test.benchmarkId(), )
        self.benchmark_test = test
        self.benchmark_data = []
        self.benchmark_start_time = time.time()
        self.benchmark_notify_time = self.benchmark_start_time
        self.benchmark_notify_block = 0

    def stopBenchmark(self, test):
        print>>self.stream, "%s ... done (%.2fs)" % ( test.benchmarkId(), time.time() - self.benchmark_start_time )

        benchmark = os.path.join(self.benchmark, test.benchmarkId())
        try:
            os.mkdir(benchmark)
        except OSError:
            print>>self.stream, "warning: cannot create %s directory" % ( test.benchmarkId(), )
        else:
            import json
            try:
                with gzip.open(os.path.join(benchmark, "data.json.gz"), "w") as jsonfile:
                    json.dump(OrderedDict(( ( "measurements", test.measurement_class.description ), ( "data", self.benchmark_data ) )),
                        jsonfile, indent=4, separators=( ",", ":" ))
            except Exception:
                print>>self.stream, traceback.format_exc()
                print>>self.stream, "warning: cannot save data for %s" % ( test.benchmarkId(), )
        finally:
            self.benchmark_test = None
            del self.benchmark_data

    def startBenchmarkBlock(self, test, block_id):
        self.benchmark_block = []
        self.benchmark_block_errors = 0

    def stopBenchmarkBlock(self, test, block_id):
        self.benchmark_data.append({ "block":block_id, "errors":self.benchmark_block_errors, "records":self.benchmark_block })
        now = time.time()
        if now - self.benchmark_notify_time >= self.benchmark_notify_interval \
                or block_id - self.benchmark_notify_block >= self.benchmark_test.benchmark_blocks * self.benchmark_notify_percent:
            self.benchmark_notify_time = now
            self.benchmark_notify_block = block_id
            print>>self.stream, "%s ... (%d of %d in %.2fs)" \
                % ( self.benchmark_test.benchmarkId(), block_id, self.benchmark_test.benchmark_blocks, now - self.benchmark_start_time )
        try:
            return self.benchmark_block_errors
        finally:
            self.benchmark_block = None
            del self.benchmark_block_errors

    def addBenchmarkRecord(self, test, record={}):
        if self.benchmark_test:
            record["test"] = test._testMethodName
            self.benchmark_block.append(record)

    def startTest(self, test):
        if not self.benchmark_test or self.verbosity > 1:
            print>>self.stream, "%s ... " % ( test.id(), ),
        super(TestResult, self).startTest(test)
        self.start_time = time.time()

    def stopTest(self, test):
        super(TestResult, self).stopTest(test)
        if not self.benchmark_test or self.verbosity > 1:
            print>>self.stream, "(%.2fs)" % ( time.time() - self.start_time, )
        if hasattr(self, "fail_args"):
            # log to self.stream using a format similar to GCC
            level, test, err = self.fail_args
            del self.fail_args
            if isinstance(err, str):
                print>>self.stream, "%s: %s %s" % ( level, test.id(), err )
            else: # traceback
                exc = getattr(err[1], "exception", err[1])
                tb = getattr(err[1], "traceback", traceback.extract_tb(err[2]))
                if tb:
                    file, line, function, text = tb[-1]
                    for f in reversed(tb):
                        if not (f[2].startswith("assert") or f[2] == "fail"):
                            file, line, function, text = f
                            break
                    print>>self.stream, "%s:%d: %s: %s failed: %s" % ( file, line, level, test.id(), exc )
                else:
                    print>>self.stream, "%s: %s failed: %s" % ( level, test.id(), exc )

    def addSuccess(self, test):
        super(TestResult, self).addSuccess(test)
        if not self.benchmark_test or self.verbosity > 1:
            print>>self.stream, "ok",

    def addError(self, test, err):
        super(TestResult, self).addError(test, err)
        print>>self.stream, "error",
        self.fail("error", test, err)
        if self.benchmark_block is not None:
            self.benchmark_block_errors += 1

    def addFailure(self, test, err):
        super(TestResult, self).addFailure(test, err)
        print>>self.stream, "fail",
        self.fail("error", test, err)
        if self.benchmark_block is not None:
            self.benchmark_block_errors += 1

    def addSkip(self, test, reason):
        super(TestResult, self).addSkip(test, reason)
        msg = "skipped: %s" % ( reason, )
        print>>self.stream, msg,
        self.fail("warning", test, msg)

    def addExpectedFailure(self, test, err):
        super(TestResult, self).addExpectedFailure(test, err)
        print>>self.stream, "expected failure",

    def addUnexpectedSuccess(self, test):
        super(TestResult, self).addUnexpectedSuccess(test)
        print>>self.stream, "unexpected success",
        self.fail("error", test, "unexpected success")
        if self.benchmark_block is not None:
            self.benchmark_block_errors += 1

    def _exc_info_to_string(self, err, test):
        # slightly modified from unittest.result to use issubclass to test exctype to be able to recognize the QCInput wrapper
        exctype, value, tb = err
        # skip test runner traceback levels
        while tb and self._is_relevant_tb_level(tb):
            tb = tb.tb_next

        if issubclass(exctype, test.failureException):
            # skip assert*() traceback levels
            length = self._count_relevant_tb_levels(tb)
            msgLines = traceback.format_exception(exctype, value, tb, length)
        else:
            msgLines = traceback.format_exception(exctype, value, tb)

        if self.buffer:
            output = sys.stdout.getvalue()
            error = sys.stderr.getvalue()
            if output:
                if not output.endswith("\n"):
                    output += "\n"
                msgLines.append(STDOUT_LINE % output)
            if error:
                if not error.endswith("\n"):
                    error += "\n"
                msgLines.append(STDERR_LINE % error)
        return "".join(msgLines)

    def addProfiler(self, test, profiler):
        import pstats
        stats = pstats.Stats(profiler, stream=self.stream).strip_dirs().sort_stats("time")
        stats.print_stats()
        stats.print_callees()
        stats.print_callers()

    def printErrors(self):
        for label, errors in ( ( "ERROR", self.errors ), ( "FAIL", self.failures ) ):
            for test, err in errors:
                print>>self.stream, "=" * 70
                print>>self.stream, "%s: %s" % ( label, test )
                print>>self.stream, "-" * 70
                print>>self.stream, "%s" % ( err, )


class TestCase(unittest.TestCase):
    def run(self, result):
        self.runTestCase(result)

    def runTestCase(self, result):
        if getattr(result, "profile", False):
            import cProfile
            profiler = cProfile.Profile()
            profiler.runcall(super(TestCase, self).run, result)
            result.addProfiler(self, profiler)
        else:
            super(TestCase, self).run(result)


class BenchmarkTooManyFailureError(Exception):
    pass


class Benchmark(unittest.TestCase):
    benchmark_blocks = 31
    benchmark_max_errors = 3
    measurement_class = NullMeasurement

    def __init__(self, methodName="runBenchmark", *args, **kwargs):
        # unittest.TestCase checks for valid methodName
        super(Benchmark, self).__init__(methodName=methodName, *args, **kwargs)
        self.benchmark_tests = getTestCaseNames(self)

    def run(self, result):
        if self._testMethodName == "runBenchmark":
            self.runBenchmark(result)
        else:
            self.runTestCase(result)

    def setResult(self, result):
        self.result = result
        if result.benchmark:
            if result.tabulate_objects:
                self.benchmark_blocks = 1
            if result.detailed_measurements and result.tabulate_objects:
                self.measurement_class = DetailedTabulatedMeasurement
            elif result.detailed_measurements:
                self.measurement_class = DetailedMeasurement
            elif result.tabulate_objects:
                self.measurement_class = TabulatedMeasurement
            elif result.time_only:
                self.measurement_class = TimeOnlyMeasurement
            else:
                self.measurement_class = Measurement

    def runTestCase(self, result):
        if result is None:
            raise ValueError("Benchmark.runTestCase result must not be None")
        self.setResult(result)
        super(Benchmark, self).run(result)

    def runBenchmark(self, result):
        if result is None:
            raise ValueError("Benchmark.runBenchmark result must not be None")
        self.setResult(result)
        tests = list(self.benchmark_tests)
        result.startBenchmark(self)
        try:
            # block randomized
            count = 0
            errors = 0
            limit = self.benchmark_blocks
            from random import shuffle
            while count < limit:
                shuffle(tests)
                try:
                    result.startBenchmarkBlock(self, count)
                    for test in tests:
                        self.__class__(test).runTestCase(result)
                finally:
                    block_errors = result.stopBenchmarkBlock(self, count)
                count += 1
                if block_errors:
                    errors += 1
                    if errors > self.benchmark_max_errors:
                        raise BenchmarkTooManyFailureError(
                            "too many blocks with errors (%d block(s) with errors, %d completed block(s) out of %d total)"
                                % ( errors, count - errors, self.benchmark_blocks ))
                    limit += 1
        except BenchmarkTooManyFailureError:
            result.addError(self, sys.exc_info())
        finally:
            result.stopBenchmark(self)

    def measure(self, *args, **kwargs):
        return self.measurement_class(self, *args, **kwargs)

    def measure_group(self, *args, **kwargs):
        return MeasurementGroup(self, *args, **kwargs)

    @classmethod
    def benchmarkId(cls):
        return "%s.%s" % ( cls.__module__, cls.__name__ )

    @classmethod
    def summarize(cls, data, stream, dir):
        statistics = OrderedDict()
        benchmark_measurements = data["measurements"]
        benchmark_data = data["data"]
        try:
            for block in benchmark_data:
                if not block["errors"]:
                    for record in block["records"]:
                        if "measurements" in record:
                            label = statistics.setdefault(record["label"], OrderedDict())
                            for measurement, value in sorted(record["measurements"].iteritems()):
                                label.setdefault(measurement, OrderedDict()).setdefault(record["test"], []).append(value)

            for label, measurements in statistics.iteritems():
                print>>stream, "***** %s *****" % ( label, )
                for measurement, description in benchmark_measurements.iteritems():
                    if description["type"] in ( "ordinal", "interval", "ratio" ):
                        measurements[measurement] = cls.summarizeNumericMeasurements(
                            measurement, description["unit"], measurements[measurement], stream, dir)
                    elif description["type"] == "tabular" and description["subtype"] in ( "ordinal", "interval", "ratio" ):
                        measurements[measurement] = cls.summarizeTabularMeasurements(
                            measurement, description["unit"], measurements[measurement], stream, dir)
        except Exception:
            print>>stream, traceback.format_exc()
            print>>stream, "warning: unexpected error summarizing statistics for %s" % ( cls.benchmarkId(), )
        finally:
            return OrderedDict(( ( "measurement-statistics", statistics ), ))

    @classmethod
    def summarizeNumericMeasurements(cls, measurement, unit, tests_values, stream, dir):
        from math import isnan
        from scipy import array, stats
        from bisect import bisect_right, bisect_left
        from itertools import imap, islice

        limit = 5
        statistics = OrderedDict(sorted(tests_values.iteritems()))
        others = dict()
        for test, values in tests_values.items():
            values.sort()
            if len(values) < limit:
                others[test] = values
                del tests_values[test]
            else:
                tests_values[test] = array(values)

        stream.write("    %s (%s)" % ( measurement, unit )) # print adds a trailing space
        if len(tests_values) >= 2:
            try:
                H, p = stats.kruskal(*tests_values.itervalues())
            except ValueError as e:
                if e.message != "All numbers are identical in kruskal":
                    raise
                H = p = float("nan")
            df = len(tests_values) - 1
            N = sum( len(values) for values in tests_values.itervalues() )
            statistics.update(( ( "H-statistic", H ), ( "p-value", p ), ( "df", df ), ( "N", N ) ))
            print>>stream, ": H(df=%d, N=%d)=%.2g, p=%.2g%s (n>=%d)" \
                % ( df, N, H, p, "**" if p < 0.01 else "*" if p < 0.05 else "", limit )
        else:
            print>>stream, ""

        tests_values_pairs = sorted(tests_values.items())

        # tests with sufficient samples
        if tests_values:
            print>>stream, "        %40s (n>=%5d): %9s [ %9s, %9s, %9s ] %9s (S=%9s; R=%9s)" \
                % ( "***** test *****", limit, "0%", "25%", "50%", "75%", "100%", "SIQR", "range" )
            for test, values in tests_values_pairs:
                count = len(values)
                minimum = min(values)
                maximum = max(values)
                value_range = maximum - minimum
                lower = stats.scoreatpercentile(values, 25)
                median = stats.scoreatpercentile(values, 50)
                upper = stats.scoreatpercentile(values, 75)
                siqr = (upper - lower) * 0.5
                print>>stream, "        %40s (n= %5d): %9.3g [ %9.3g, %9.3g, %9.3g ] %9.3g (S=%9.3g; R=%9.3g)" \
                    % ( test, count, minimum, lower, median, upper, maximum, siqr, value_range )
                statistics[test] = OrderedDict((
                    ( "count", count ), ( "min", minimum ), ( "lower", lower ), ( "median", median ), ( "upper", upper ), ( "max", maximum ),
                    ( "siqr", siqr ), ( "range", value_range )
                ))

            # post-hoc multiple comparison
            if len(tests_values) >= 3:
                print>>stream, "        Post-hoc pairwise Mann-Whitney U with Holm correction (Cliff's delta effect size)"
                def mannwhitneyu(x, y):
                    try:
                        return stats.mannwhitneyu(x, y)
                    except ValueError as e:
                        if e.message != "All numbers are identical in amannwhitneyu":
                            raise
                        return ( float("nan"), float("nan") )
                pairwise_pvalue = sorted(
                    ( ( ( test, other_test ), mannwhitneyu(values, other_values)[1] * 2 )
                        for i, ( test, values ) in enumerate(tests_values_pairs, 1)
                        for other_test, other_values in islice(tests_values_pairs, i, None) ),
                    key=lambda x: x[1])
                def delta(values, other_values):
                    d = 0
                    i = 0
                    n = 0
                    while i < len(values):
                        x = values[i]
                        j = bisect_right(values, x, i)
                        m = bisect_left(other_values, x, n)
                        n = bisect_right(other_values, x, m)
                        d += (j - i) * (m - len(other_values) + n)
                        i = j
                    # assert d == sum( cmp(x, y) for x in values for y in other_values )
                    return d / float(len(values) * len(other_values))
                pairwise_delta = dict(
                    ( ( test, other_test ), delta(values, other_values) )
                        for i, ( test, values ) in enumerate(tests_values_pairs, 1)
                        for other_test, other_values in islice(tests_values_pairs, i, None) )
                adjusted = [ float("nan") if isnan(p) else min(1, (len(pairwise_pvalue) - i) * p) for i, ( _, p ) in enumerate(pairwise_pvalue) ]
                pairwise_corrected = { pair: float("nan") if isnan(p) else max(islice(adjusted, i)) for i, ( pair, p ) in enumerate(pairwise_pvalue, 1) }
                for i, ( test, _ ) in enumerate(tests_values_pairs, 1):
                    pd = ( ( pairwise_corrected[test, other_test], pairwise_delta[test, other_test] ) for other_test, _ in islice(tests_values_pairs, i, None) )
                    row = " ".join( "%9.3g(%6.3f)%-2s" % ( p, d, "**" if p < 0.01 else "*" if p < 0.05 else "" ) for p, d in pd )
                    print>>stream, ("            %s%40s   %s" % ( " " * 20 * i, test, row )).rstrip()
                statistics["post-hoc"] = OrderedDict((
                    ( "test", "mann-whitney" ),
                    ( "pairwise-pvalue", sorted(pairwise_pvalue) ),
                    ( "correction", "holm" ),
                    ( "pairwise-corrected", sorted(pairwise_corrected.items()) ),
                    ( "effect-size", "cliffs-delta" ),
                    ( "pairwise-effect-size", sorted(pairwise_delta.items()) )
                ))

        # tests with insufficient samples
        if others:
            print>>stream, "        %40s (n< %5d): { ... values ... } (R=%9s)" % ( "***** test *****", limit, "range" )
            for test, values in others.iteritems():
                count = len(values)
                value_range = values[-1] - values[0]
                print>>stream, "            %40s (n= %5d): {" % ( test, count ),
                for value in values:
                    print>>stream, "%9.3g" % ( value, ),
                print>>stream, "} (R=%9.3g)" % ( value_range, )
                statistics[test] = OrderedDict(( ( "count", count ), ( "values", values ), ( "range", value_range ) ))

        return statistics

    @classmethod
    def summarizeTabularMeasurements(cls, measurement, unit, tests_values, stream, dir):
        statistics = OrderedDict()

        print>>stream, "    %s (average %s)" % ( measurement, unit  )
        for test, values in sorted(tests_values.iteritems()):
            print>>stream, "        %s (n= %5d)" % ( test, len(values) )
            total = Counter()
            for value in values:
                total.update(value)
            averaged = OrderedDict()
            average_total = 0.
            for key, value in sorted(total.iteritems(), key=lambda kv: kv[1], reverse=True):
                average = value / float(len(values))
                averaged[key] = average
                average_total += average
                print>>stream, "            %9.3g: %s" % ( average, key )
            print>>stream, "            %9.3g: ***** total *****" % ( average_total, )
            statistics[test] = averaged

        return statistics



class ImportTestFailure(unittest.TestCase):
    def __init__(self, test_name):
        super(ImportTestFailure, self).__init__()
        self.test_name = test_name
        exc_info = sys.exc_info()
        tb = exc_info[2]
        while tb and "__unittest" in tb.tb_frame.f_globals:
            tb = tb.tb_next
        self.exception = exc_info[0]("".join(traceback.format_exception(exc_info[0], exc_info[1], tb)))

    def runTest(self):
        raise self.exception

    def id(self):
        return "import %s" % ( self.test_name, )


class LoadTestsFailure(ImportTestFailure):
    def id(self):
        return "%s.load_test" % ( self.test_name, )


class TestLoader(unittest.TestLoader):
    """
    Test loader that also handles generated modules and package submodules.
    """
    def getTestCaseNames(self, testCaseClass):
        return getTestCaseNames(testCaseClass)

    def loadTestsFromModule(self, module, use_load_tests=True, visited=set()):
        if module is None:
            module =__import__("__main__")
        tests = self.suiteClass()
        if module in visited:
            return tests
        visited.add(module)

        # treat __main__ as the top-level module
        if module.__name__ == "__main__":
            prefix = ""
            submodule_paths = ( os.path.dirname(module.__file__), )
        else:
            prefix = module.__name__ + "."
            submodule_paths = getattr(module, "__path__", ())

        # import package submodules
        for submodule_path in submodule_paths:
            paths = os.listdir(submodule_path)
            for path in paths:
                file, ext = os.path.splitext(path)
                full_path = os.path.join(submodule_path, path)
                if not testmodule_re.match(file) \
                        or ext != ".py" if os.path.isfile(full_path) \
                            else not os.path.isfile(os.path.join(full_path, "__init__.py")):
                    continue
                name = prefix + file
                try:
                    submodule = __import__(name, fromlist=( "*", ))
                except:
                    tests.addTest(ImportTestFailure(name))
                else:
                    tests.addTests(self.loadTestsFromModule(submodule, use_load_tests, visited=visited))

        # load test cases and test modules from module properties
        from types import ModuleType
        if not hasattr(module, "__unittest"):
            for name, obj in module.__dict__.iteritems():
                if isinstance(obj, type) and issubclass(obj, unittest.TestCase):
                    tests.addTests(self.loadTestsFromTestCase(obj))
                elif isinstance(obj, ModuleType) and testmodule_re.match(name):
                    tests.addTests(self.loadTestsFromModule(obj, use_load_tests, visited=visited))

        # unittest load_test protocol
        load_tests = getattr(module, "load_tests", None)
        if use_load_tests and load_tests is not None:
            try:
                return load_tests(self, tests, None)
            except:
                return self.suiteClass([ LoadTestsFailure(module.__name__) ])
        return tests

    def loadTestsFromName(self, name, module=None):
        try:
            return super(TestLoader, self).loadTestsFromName(name, module=module)
        except:
            return self.suiteClass([ ImportTestFailure(name) ])


class ImportBenchmarkFailure(ImportTestFailure):
    pass


class BenchmarkLoader(TestLoader):
    def loadTestsFromTestCase(self, testCaseClass):
        if issubclass(testCaseClass, Benchmark) :
            test = testCaseClass()
            if test.benchmark_tests:
                return self.suiteClass([ test ])
        return self.suiteClass()

    def loadTestsFromNames(self, names, module=None):
        suite = self.suiteClass()
        for name in names:
            for test in self.loadTestsFromName(name, module):
                if not isinstance(test, ( Benchmark, ImportTestFailure )) or isinstance(test, Benchmark) and test._testMethodName != "runBenchmark":
                    try:
                        raise ValueError("%s is not a benchmark" % ( name, ))
                    except:
                        suite.addTest(ImportBenchmarkFailure(name))
                else:
                    suite.addTest(test)
        return suite


def parameterized_test_module(moduledict=None, arg=None, **kwargs):
    def helper(fn):
        import sys, types

        # the following uses cpython __builtins__ to mimic an extension to the fn closure and avoid polluting the testmodule globals;
        # note that using a dict subclass as the globals for types.FunctionType does not work, since cpython uses PyDict_{Get,Set}Item

        if moduledict is not None:
            old_builtins = moduledict["__builtins__"]
            moduledict["__builtins__"] = dict(fn.__globals__["__builtins__"])
            moduledict["__builtins__"].update(fn.__globals__)
            types.FunctionType(fn.__code__, moduledict, fn.__name__, fn.__defaults__, fn.__closure__)(arg)
            moduledict["__builtins__"] = old_builtins

        for kwname, kwarg in kwargs.iteritems():
            testname = "Test%s" % ( kwname, )
            testfqn = "%s.%s"% ( fn.__module__, testname )
            try:
                testmodule = __import__(testfqn, fromlist=( "*", ))
            except ImportError:
                testmodule = types.ModuleType(testfqn)
                sys.modules[testfqn] = testmodule
            testmodule.__builtins__ = dict(fn.__globals__["__builtins__"])
            testmodule.__builtins__.update(fn.__globals__)
            if "__file__" in fn.__globals__:
                testmodule.__file__ = fn.__globals__["__file__"]
            types.FunctionType(fn.__code__, testmodule.__dict__, fn.__name__, fn.__defaults__, fn.__closure__)(kwarg)
            fn.__globals__[testname] = testmodule
    return helper


class TestProgram(object):
    def __init__(self, argv=sys.argv, stream=sys.stderr, verbosity=1, failfast=None, catchbreak=None, buffer=None,
            profile=None, benchmark=None, time_only=None, detailed_measurements=None, tabulate_objects=None, resummarize=None):
        self.stream = stream
        self.verbosity = verbosity
        self.failfast = failfast
        self.catchbreak = catchbreak
        self.buffer = buffer
        self.profile = profile
        if benchmark and resummarize:
            raise ValueError("TestProgram only one of benchmark or resummarize may be True")
        self.benchmark = benchmark
        if benchmark is False and (time_only or detailed_measurements or tabulate_objects):
            raise ValueError("TestProgram time_only, detailed_measurements, and tabulate_objects may be True only if benchmark is not False")
        if time_only and (detailed_measurements or tabulate_objects):
            raise ValueError("TestProgram detailed_measurements, and tabulate_objects may be True only if time_only is not True")
        self.time_only = time_only
        self.detailed_measurements = detailed_measurements
        self.tabulate_objects = tabulate_objects
        self.resummarize = resummarize

        self.parseArgs(argv)
        self.createTests()
        if self.resummarize is None:
            error = self.runTests()
        else:
            error = self.summarize(self.resummarize)
        sys.exit(error)

    def parseArgs(self, argv):
        import argparse
        parser = argparse.ArgumentParser(prog=os.path.basename(argv[0]))
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--verbose", help="verbose output", action="store_const", const=2, dest="verbosity")
        group.add_argument("-d", "--debug", help="debugging output", action="store_const", const=3, dest="verbosity")
        group.add_argument("-q", "--quiet", help="minimal output", action="store_const", const=0, dest="verbosity")
        if self.failfast is None:
            parser.add_argument("-f", "--failfast", help="stop on first failure", action="store_true")
        if self.catchbreak is None:
            parser.add_argument("-c", "--catchbreak", help="catch control-C and display results", action="store_true")
        if self.buffer is None:
            parser.add_argument("-b", "--buffer", help="buffer stdout and stderr during test runs", action="store_true")
        if self.profile is None and self.benchmark is None and self.resummarize is None:
            group = parser.add_mutually_exclusive_group()
            group.add_argument("-p", "--profile", help="enable profiling for supported tests", action="store_true")
            group.add_argument("-B", "--benchmark", metavar="directory",
                help="run benchmark on supported tests and store the results in %(metavar)s (default: \"%(const)s\")", nargs="?", const="Results")
            group.add_argument("-R", "--resummarize", metavar="directory",
                help="resummarize benchmark data in %(metavar)s (default: \"%(const)s\")", nargs="?", const="Results/latest")
        if self.benchmark is not False:
            if self.time_only is None and (self.detailed_measurements is not True and self.tabulate_objects is not True):
                parser.add_argument("-T", "--time-only", help="take more detailed measurements in benchmarks", action="store_true")
            if self.time_only is not True and self.detailed_measurements is None:
                parser.add_argument("-D", "--detailed-measurements", help="take more detailed measurements in benchmarks", action="store_true")
            if self.time_only is not True and self.tabulate_objects is None:
                parser.add_argument("-O", "--tabulate-objects",
                    help="run only one block of each benchmark to tabulate sizes of objects during measurements", action="store_true")
        parser.add_argument("testNames", help="specific tests to run if given", metavar="test", nargs="*")
        parser.parse_args(argv[1:], self)

        if self.benchmark is not None:
            if os.path.exists(self.benchmark) and not os.path.isdir(self.benchmark):
                parser.error("--benchmark %s exists but is not a directory" % ( self.benchmark, ))
        elif self.resummarize is not None:
            if not os.path.exists(self.resummarize):
                parser.error("--resummarize %s does not exist" % ( self.resummarize, ))
            elif not os.path.isdir(self.resummarize):
                parser.error("--resummarize %s exists but is not a directory"  % ( self.resummarize, ))

        if self.time_only:
            if self.detailed_measurements:
                parser.error("argument -T/--time-only: not allowed with argument -D/--detailed-measurements")
            elif self.tabulate_objects:
                parser.error("argument -T/--time-only: not allowed with argument -O/--tabulate-objects")

        if not self.testNames:
            self.testNames = None
        if self.verbosity < 3:
            os.environ.pop("QC_VERBOSE", None)
        else:
            os.environ["QC_VERBOSE"] = "1"

    def createTests(self):
        if self.benchmark is not None or self.resummarize is not None:
            testloader = BenchmarkLoader()
        else:
            testloader = TestLoader()
        if self.testNames is None:
            self.tests = testloader.loadTestsFromModule(None)
        else:
            self.tests = testloader.loadTestsFromNames(self.testNames, None)

    def runTests(self):
        if self.catchbreak:
            unittest.installHandler()

        class TestRunner(unittest.TextTestRunner):
            def _makeResult(inner):
                return TestResult(inner.stream, inner.descriptions, inner.verbosity, self.profile, benchmark=self.benchmark,
                    time_only=self.time_only, detailed_measurements=self.detailed_measurements, tabulate_objects=self.tabulate_objects)
        testRunner = TestRunner(stream=self.stream, verbosity=self.verbosity, failfast=self.failfast, buffer=self.buffer)

        self.result = testRunner.run(self.tests)
        error = not self.result.wasSuccessful()
        if self.benchmark is not None:
            error = error or self.summarize(os.path.join(self.benchmark, "latest"))
        return error

    def summarize(self, dir):
        error = False
        import json
        for test in self.tests:
            print>>self.stream, "Summarizing %s ..."  % ( test.benchmarkId(), )
            benchmark = os.path.join(dir, test.benchmarkId())
            try:
                with gzip.open(os.path.join(benchmark, "data.json.gz")) as jsonfile:
                    benchmark_data = json.load(jsonfile, object_pairs_hook=OrderedDict)
            except Exception:
                print>>self.stream, traceback.format_exc()
                print>>self.stream, "warning: error loading data to summarize for %s" % ( test.benchmarkId(), )
                error = True
            else:
                try:
                    txtfile = open(os.path.join(benchmark, "summary.txt"), "w+")
                except Exception:
                    print>>self.stream, traceback.format_exc()
                    print>>self.stream, "warning: cannot save summary for %s" % ( test.benchmarkId(), )
                    try:
                        test.summarize(benchmark_data, self.stream, benchmark)
                    except Exception:
                        print>>self.stream, traceback.format_exc()
                        print>>self.stream, "warning: error creating summary for %s" % ( test.benchmarkId(), )
                    error = True
                else:
                    with txtfile:
                        try:
                            summary = test.summarize(benchmark_data, txtfile, benchmark)
                        except Exception:
                            print>>self.stream, traceback.format_exc()
                            print>>self.stream, "warning: error creating summary for %s" % ( test.benchmarkId(), )
                            error = True
                        else:
                            try:
                                with gzip.open(os.path.join(benchmark, "summary.json.gz"), "w") as jsonfile:
                                    json.dump(summary, jsonfile, indent=4, separators=( ",", ":" ))
                            except Exception:
                                print>>self.stream, traceback.format_exc()
                                print>>self.stream, "warning: error saving summary for %s" % ( test.benchmarkId(), )
                                error = True
                        finally:
                            txtfile.seek(0)
                            self.stream.writelines( "    " + line for line in txtfile )
        return error
