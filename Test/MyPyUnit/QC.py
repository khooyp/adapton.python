# Copyright (c) 2009-2011, Dan Bravender <dan.bravender@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# From https://github.com/dbravender/qc

import functools, itertools, os, pprint, random, sys

__all__ = (
    "always", "choices", "samples", "permutations", "combinations",
    "booleans", "integers", "floats", "lists", "tuples", "unicodes", "characters", "objects",
    "forall", "every", "where", "include"
)

__unittest = True


def always(x):
    while True:
        yield x

def choices(xs):
    xs = list(xs)
    for x in xs:
        yield x
    while True:
        yield random.choice(xs)

def samples(xs=xrange(0, 101), size=None):
    xs = list(xs)
    if size is None:
        size = (0, len(xs))
    else:
        size = (max(0, size[0]), min(len(xs), size[1]))
    yield xs
    yield list(reversed(xs))
    while True:
        yield random.sample(xs, random.randint(size[0], size[1]))

def permutations(xs=xrange(0, 101)):
    xs = list(xs)
    yield xs
    yield list(reversed(xs))
    while True:
        yield random.sample(xs, len(xs))

def combinations(xs=xrange(0, 101), size=None):
    xs = list(xs)
    if size is None:
        size = (0, len(xs))
    else:
        size = (max(0, size[0]), min(len(xs), size[1]))
    yield xs
    while True:
        yield [ xs[k] for k in sorted(random.sample(xrange(len(xs)), random.randint(size[0], size[1]))) ]

def booleans():
    yield True
    yield False
    while True:
        yield random.choice(( True, False ))

def integers(low=0, high=100):
    """
    Endlessly yields random integers between (inclusively) low and high.
    Yields low then high first to test boundary conditions.
    """
    yield low
    yield high
    while True:
        yield random.randint(low, high)

def floats(low=0.0, high=100.0):
    """
    Endlessly yields random floats between (inclusively) low and high.
    Yields low then high first to test boundary conditions.
    """
    yield low
    yield high
    while True:
        yield random.uniform(low, high)

def lists(items=integers(), size=( 0, 100 )):
    """
    Endlessly yields random lists varying in size between size[0]
    and size[1]. Yields a list of the low size and the high size
    first to test boundary conditions.
    """
    yield [ items.next() for _ in xrange(size[0]) ]
    yield [ items.next() for _ in xrange(size[1]) ]
    while True:
        yield [ items.next() for _ in xrange(random.randint(size[0], size[1])) ]

def tuples(*elements):
    """
    Endlessly yields random tuples with the given elements.
    """
    while True:
        yield tuple( el.next() for el in elements )

def key_value_generator(keys=integers(), values=integers()):
    while True:
        yield ( keys.next(), values.next() )

def dicts(key_values=key_value_generator(), size=( 0, 100 )):
    while True:
        x = {}
        for _ in xrange(random.randint(size[0], size[1])):
            item, value = key_values.next()
            while item in x:
                item, value = key_values.next()
            x.update({ item: value })
        yield x

def unicodes(minunicode=0, maxunicode=255, size=( 0, 100 )):
    for r in ( size[0], size[1] ):
        yield "".join(unichr(random.randint(minunicode, maxunicode)) for _ in xrange(r))
    while True:
        yield "".join(unichr(random.randint(minunicode, maxunicode)) for _ in xrange(random.randint(size[0], size[1])))

characters = functools.partial(unicodes, size=( 1, 1 ))

def objects(_object_class, _fields={}, *init_args, **init_kwargs):
    """
    Endlessly yields objects of given class, with fields specified
    by given dictionary. Uses given constructor arguments while creating
    each object.
    """
    while True:
        ctor_args = ( arg.next() for arg in init_args )
        ctor_kwargs = { k: v.next() for k, v in init_kwargs.iteritems() }
        obj = _object_class(*ctor_args, **ctor_kwargs)
        for k, v in _fields.iteritems():
            setattr(obj, k, v.next())
        yield obj

class QCInput(Exception):
    _qcinput_class_cache = {}

    def __new__(cls, exc_type, exc_value, input):
        # subclass exc_type so that the QCInput wrapper passes isinstance/issubclass checks
        try:
            qcinput_class = cls._qcinput_class_cache[exc_type]
        except KeyError as e:
            qcinput_class = cls._qcinput_class_cache[exc_type] = type("%s(%s)" % ( cls.__name__, exc_type.__class__.__name__ ), ( exc_type, cls ), {})
        self = super(QCInput, cls).__new__(qcinput_class) # skip exc_type.__new__
        self.exc_type = exc_type
        self.exc_value = exc_value
        self.input = input
        return self

    def __init__(self, exc_type, exc_value, input):
        pass # suppress exc_type's __init__

    def __str__(self):
        try:
            inputstr = pprint.pformat(self.input)
        except Exception as e:
            inputstr = "Exception raised during input:\n%s" % ( e, )
        import traceback
        return "On input:\n%s\n\n%s" % ( inputstr, "".join(traceback.format_exception_only(self.exc_type, self.exc_value)) )

class Discard(Exception):
    pass

def forall(tries=100, **kwargs):
    def wrap(f):
        @functools.wraps(f)
        def wrapped(*inargs, **inkwargs):
            attempt = 0
            @apply
            def gen_random_kwargs():
                while True:
                    yield { name: gen.next() for ( name, gen ) in kwargs.iteritems() }
            if hasattr(f, "qc_include"):
                attempt -= len(f.qc_include)
                gen_random_kwargs = itertools.chain(f.qc_include, gen_random_kwargs)
            while attempt < tries:
                random_kwargs = gen_random_kwargs.next()
                if forall.verbose or os.environ.has_key("QC_VERBOSE"):
                    pprint.pprint(random_kwargs, sys.stderr)
                random_kwargs.update(**inkwargs)
                try:
                    f(*inargs, **random_kwargs)
                except Discard:
                    pass
                except QCInput as e: # for nested forall/every
                    e.input.update(random_kwargs)
                    raise
                except Exception:
                    exc_info = sys.exc_info()
                    raise QCInput(exc_info[0], exc_info[1], random_kwargs), None, exc_info[2]
                else:
                    attempt += 1
        return wrapped
    return wrap
forall.verbose = False # if enabled will print out the random test cases

def every(**kwargs):
    def wrap(f):
        @functools.wraps(f)
        def wrapped(*inargs, **inkwargs):
            for values in itertools.product(*kwargs.itervalues()):
                one_kwargs = dict(zip(kwargs.iterkeys(), values))
                if forall.verbose or os.environ.has_key("QC_VERBOSE"):
                    pprint.pprint(one_kwargs, sys.stderr)
                one_kwargs.update(**inkwargs)
                try:
                    f(*inargs, **one_kwargs)
                except QCInput as e: # for nested forall/every
                    e.input.update(random_kwargs)
                    raise
                except Exception:
                    exc_info = sys.exc_info()
                    raise QCInput(exc_info[0], exc_info[1], one_kwargs), None, exc_info[2]
        return wrapped
    return wrap

def where(pred):
    def wrap(f):
        @functools.wraps(f)
        def wrapped(*inargs, **inkwargs):
            if not pred(**inkwargs):
                if forall.verbose or os.environ.has_key("QC_VERBOSE"):
                    print>>sys.stderr, "...discarded"
                raise Discard()
            f(*inargs, **inkwargs)
        return wrapped
    return wrap

def include(**kwargs):
    def wrap(f):
        if not hasattr(f, "qc_include"):
            f.qc_include = []
        f.qc_include.append(kwargs)
        return f
    return wrap
