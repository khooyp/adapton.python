import MyPyUnit
from MyPyUnit import QC
from AdaptonTestApps.ListOps import *
from AdaptonTestApps.FastListOps import *


@MyPyUnit.parameterized_test_module(EagerListOps=EagerListOps, LazyListOps=LazyListOps, EagerModListOps=EagerModListOps,
    LazyModListOps=LazyModListOps, BidirectionalLazyModListOps=BidirectionalLazyModListOps,
    FastBidirectionalLazyModListOps=FastBidirectionalLazyModListOps, DebugLazyModListOps=DebugLazyModListOps)
def make_list_ops_test_case(ListOps):
    global Correctness, Regression

    class Correctness(MyPyUnit.TestCase):
        @QC.forall(
            xs=QC.lists(size=( 10, 20 )),
            edits=QC.lists(size=( 2, 5 ), items=QC.lists(size=( 2, 5 ), items=QC.tuples(QC.booleans(), QC.integers(), QC.integers()))))
        @QC.include(xs=[ 1, 2, 3 ], edits=[ [ ( False, 2, 0 ), ( False, 0, 0 ) ] ])
        @QC.include(xs=[ 1 ], edits=[ [ ( True, 0, 2 ), ( False, 0, 3 ) ], [ ( True, 1, 4 ) ] ])
        @QC.include(xs=[ 1 ], edits=[ [ ( True, 0, 2 ) ], [ ( False, 1, 3 ), ( True, 0, 4 ) ] ])
        def test_list_map_insert_delete(self, xs, edits):
            xs = list(xs)
            ys = ListOps.of_list(xs)
            yss = ListOps.map(lambda x: x + 1, ys)
            xss = map(lambda x: x + 1, xs)
            self.assertEqual(xss, ListOps.to_list(yss))

            for edit in edits:
                for op, index, value in edit:
                    index = index % len(xs)
                    if op or len(xs) == 1:
                        xs.insert(index, value)
                        zs = ListOps.insert(ys, index, value)
                    else:
                        xs.pop(index)
                        _, zs = ListOps.delete(ys, index)
                    if ListOps.self_adjusting:
                        self.assertIs(ys, zs)
                    else:
                        ys = zs
                xss = map(lambda x: x + 1, xs)
                if not ListOps.self_adjusting:
                    yss = ListOps.map(lambda x: x + 1, ys)
                self.assertEqual(xss, ListOps.to_list(yss))

        @QC.forall(xs=QC.lists())
        def test_quicksort(self, xs):
            ys = ListOps.of_list(xs)
            xss = sorted(xs)
            yss = ListOps.quicksort(ys)
            self.assertEqual(xss, ListOps.to_list(yss))

        @QC.forall(xs=QC.lists(size=( 20, 30 )))
        def test_quicksort_insert_end_max(self, xs):
            xs = list(xs)
            ys = ListOps.of_list(xs)
            xss = sorted(xs)
            yss = ListOps.quicksort(ys)
            self.assertEqual(xss, ListOps.to_list(yss))

            for _ in range(20):
                z = max(xs) + 1
                pos = len(xs)
                zs = ListOps.insert(ys, pos, z)
                xs.insert(pos, z)
                if ListOps.self_adjusting:
                    self.assertIs(ys, zs)
                else:
                    ys = zs
                    yss = ListOps.quicksort(ys)
                xss = sorted(xs)
                self.assertEqual(xss[:1], ListOps.take(yss, 1))

        @QC.forall(
            xs=QC.lists(size=( 10, 20 )),
            edits=QC.lists(size=( 2, 5 ), items=QC.lists(size=( 2, 5 ), items=QC.tuples(QC.booleans(), QC.integers(), QC.integers()))))
        @QC.include(xs=[ 1, 2, 3 ], edits=[ [ ( False, 2, 0 ), ( False, 0, 0 ) ] ])
        def test_quicksort_insert_delete(self, xs, edits):
            xs = list(xs)
            ys = ListOps.of_list(xs)
            xss = sorted(xs)
            yss = ListOps.quicksort(ys)
            self.assertEqual(xss, ListOps.to_list(yss))

            for edit in edits:
                for op, index, value in edit:
                    index = index % len(xs)
                    if op or len(xs) == 1:
                        xs.insert(index, value)
                        zs = ListOps.insert(ys, index, value)
                    else:
                        xs.pop(index)
                        _, zs = ListOps.delete(ys, index)
                    if ListOps.self_adjusting:
                        self.assertIs(ys, zs)
                    else:
                        ys = zs
                xss = sorted(xs)
                if not ListOps.self_adjusting:
                    yss = ListOps.quicksort(ys)
                self.assertEqual(xss, ListOps.to_list(yss))


    class Regression(MyPyUnit.TestCase):
        if ListOps is LazyModListOps:
            def test_list_partition_append_delete_twice(self):
                xs = ListOps.of_list([ 1, 2, 3 ])
                ys, zs = ListOps.partition(1, xs)
                rs = ListOps.concat(ys, zs)
                self.assertEqual([ 1, 2, 3 ], ListOps.to_list(rs))

                _, xs = ListOps.delete(xs, 2)
                _, xs = ListOps.delete(xs, 0)

                self.assertEqual([ 2 ] , ListOps.to_list(rs))

            def test_quicksort_1_2_3_delete_3_1_unrolled(self):
                # this tests a potential problem when lazy_inorder_depth == 0
                xs = ListOps.mod(lambda tail: ( 1, tail ), ListOps.mod(lambda tail: ( 2, tail ), ListOps.mod(lambda tail: ( 3, tail ), ListOps.mod(lambda: None))))
                def xsort(xs):
                    xh, xt = xs.force()
                    def part(xh, xt):
                        p = xt.force()
                        if p is None:
                            return ( ListOps.mod(ListOps.nil), ListOps.mod(ListOps.nil) )
                        else:
                            ph, pt = p
                            q = pt.force()
                            if q is None:
                                lrs = ( ListOps.mod(ListOps.nil), ListOps.mod(ListOps.nil) )
                            else:
                                qh, qt = q
                                lrs = ( ListOps.mod(ListOps.nil), ListOps.mod(ListOps.cons, qh, ListOps.mod(ListOps.nil)) )
                            return ( lrs[0], ListOps.mod(ListOps.cons, ph, lrs[1]) )
                    part = ListOps.mod(part, xh, xt)
                    def ys(part):
                        return part.force()[0].force()
                    ys = ListOps.mod(ys, part)
                    def zs(part):
                        return part.force()[1].force()
                    zs = ListOps.mod(zs, part)
                    self.assertIsNone(ys.force())
                    return ListOps.cons(xh, zs)
                xsort = ListOps.mod(xsort, xs)
                self.assertEqual([ 1, 2, 3 ], ListOps.to_list(xsort))

                xs.force()[1].update(lambda head, tail: ( head, tail ), xs.force()[1].force()[0], xs.force()[1].force()[1].force()[1])
                xs.update(lambda head, tail: ( head, tail ), xs.force()[1].force()[0], xs.force()[1].force()[1])

                self.assertEqual([ 2 ], ListOps.to_list(xsort))
