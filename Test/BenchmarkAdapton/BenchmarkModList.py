
from collections import defaultdict, OrderedDict
from itertools import chain, imap, izip, product, cycle
from AdaptonTestApps.ListOps import *


def initrecursionlimit():
    import sys, resource
    frame_size = 1024 # rough overestimate
    new_limit = resource.getrlimit(resource.RLIMIT_STACK)[0] // frame_size
    if new_limit < 0:
        new_limit = 2**30 - 1 # 2**31 - 1 leads to some bugs in Python
    if sys.getrecursionlimit() < new_limit:
        sys.setrecursionlimit(new_limit)


def driver(( ListOps, task, take, size, seed, edit_count )):
    try:
        import random, resource, sys, time
        from scipy import stats

        ListOps = globals()[ListOps]
        print>>sys.stderr, "%24s %24s %4d %10d %20d" % ( ListOps, task.__name__, take, size, seed )
        driver_start_time = time.time()

        results = OrderedDict((
            ( "ListOps", str(ListOps) ), ( "task", task.__name__ ), ( "take", take ), ( "size", size ), ( "seed", seed ),
            ( "units", OrderedDict(( ( "time", "seconds" ), ( "max-rss", "bytes" if sys.platform.startswith("darwin") else "kilobytes" ) )) ) ))
        rng = random.Random(seed)
        indices = xrange(size)
        xs = ListOps.mod(ListOps.nil)
        xss = []
        for _ in indices:
            x = rng.random()
            xs = ListOps.mod(ListOps.cons, x, xs)
            xss.append(xs)

        start_time = time.time()
        ys = task(ListOps, xs)
        setup_time = time.time() - start_time

        r = resource.getrusage(resource.RUSAGE_SELF)
        results.update(( ( "setup-time", setup_time ), ( "setup-max-rss", r.ru_maxrss ) ))

        start_time = time.time()
        ListOps.take(ys, take)
        take_time = time.time() - start_time

        r = resource.getrusage(resource.RUSAGE_SELF)
        results.update(( ( "take-time", take_time ), ( "take-max-rss", r.ru_maxrss ) ))

        if ListOps.self_adjusting:
            delete_times = []
            insert_times = []
            results["delete-times"] = delete_times
            results["insert-times"] = insert_times

            edits = random.sample(indices, min(size, edit_count))
            interval_time = time.time()
            for num, edit in enumerate(edits):
                next_interval_time = time.time()
                if time.time() - interval_time > 20:
                    print>>sys.stderr, "%24s %24s %4d %10d %20d edit %10d" % ( ListOps, task.__name__, take, size, seed, num )
                    interval_time = next_interval_time

                value = xss[edit].force()[0]
                tail = xss[edit].force()[1]
                start_time = time.time()
                if tail.force() is None:
                    xss[edit].update(ListOps.nil)
                else:
                    xss[edit].update(ListOps.cons, tail.force()[0], tail.force()[1])
                delete_update_time = time.time() - start_time

                start_time = time.time()
                ListOps.take(ys, take)
                delete_take_time = time.time() - start_time
                delete_times.append(( delete_update_time, delete_take_time ))

                start_time = time.time()
                xss[edit].update(ListOps.cons, value, tail)
                insert_update_time = time.time() - start_time

                start_time = time.time()
                ListOps.take(ys, take)
                insert_take_time = time.time() - start_time
                insert_times.append(( insert_update_time, insert_take_time ))

            r = resource.getrusage(resource.RUSAGE_SELF)
            delete_times = [ OrderedDict(( ( "update", u ), ( "take", t ) )) for u, t in delete_times ]
            insert_times = [ OrderedDict(( ( "update", u ), ( "take", t ) )) for u, t in insert_times ]
            results["edits"] = OrderedDict(( ( "max-rss", r.ru_maxrss ), ))
            for label, values in (
                    ( "update-stats", [ t["update"] for t in chain(delete_times, insert_times) ] ),
                    ( "take-stats", [ t["take"] for t in chain(delete_times, insert_times) ] ),
                    ( "total-stats", [ t["update"] + t["take"] for t in chain(delete_times, insert_times) ] )):
                samples = len(values)
                minimum = min(values)
                maximum = max(values)
                value_range = maximum - minimum
                lower = stats.scoreatpercentile(values, 25)
                median = stats.scoreatpercentile(values, 50)
                upper = stats.scoreatpercentile(values, 75)
                siqr = (upper - lower) * 0.5
                mean = stats.tmean(values)
                stddev = stats.tstd(values)
                results["edits"][label] = OrderedDict((
                    ( "min", minimum ), ( "lower", lower ), ( "median", median ), ( "upper", upper ), ( "max", maximum ),
                    ( "samples", samples ), ( "siqr", siqr ), ( "range", value_range ),
                    ( "mean", mean ), ( "stddev", stddev ),
                ))
            results["edits"].update(( ( "edits", edits ), ( "delete-times", delete_times ), ( "insert-times", insert_times ) ))
            del results["delete-times"], results["insert-times"]
            print>>sys.stderr, "%24s %24s %4d %10d %20d ... done (%9.2fs) %9.3gs %9.3gs %9.3gs" \
                % ( ListOps, task.__name__, take, size, seed, time.time() - driver_start_time, setup_time, take_time, mean )
        else:
            print>>sys.stderr, "%24s %24s %4d %10d %20d ... done (%9.2fs) %9.3gs %9.3gs" \
                % ( ListOps, task.__name__, take, size, seed, time.time() - driver_start_time, setup_time, take_time )
        return results

    except Exception, KeyboardInterrupt:
        import traceback
        error = traceback.format_exc()
        results["error"] = error
        print>>sys.stderr, error
        print>>sys.stderr, "%24s %24s %4d %10d %20d ... error (%9.2fs)" % ( ListOps, task.__name__, take, size, seed, time.time() - driver_start_time )
        driver_start_time = time.time()
        return results


def list_filter_task(ListOps, input):
    return ListOps.filter(lambda x: x < 0.5, input)


def list_map_task(ListOps, input):
    return ListOps.map(lambda x: x * 3 + x * 7 + x * 9, input)


def list_quicksort_task(ListOps, input):
    return ListOps.quicksort(input)


if __name__ == "__main__":
    import argparse, gzip, json, multiprocessing, os, pprint, random, sys, time, traceback

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-B", "--benchmark", metavar="DIRECTORY",
        help="run benchmark and store results in %(metavar)s (default: \"%(const)s\")", nargs="?", const="Results/ListOps")
    group.add_argument("-R", "--resummarize", metavar="DIRECTORY",
        help="resummarize benchmark data in %(metavar)s (default: \"Results/ListOps\")", nargs="*")
    parser.add_argument("-P", "--processes", metavar="N", help="run %(metavar)s benchmarks in parallel", default=multiprocessing.cpu_count(), type=int)
    parser.add_argument("-I", "--input-sizes", metavar="SIZE",
        help="run benchmarks with input list size (default: 20000 2000 200 10000 1000 100 5000 500 50)",
        nargs="+", default=( 20000, 2000, 200, 10000, 1000, 100, 5000, 500, 50 ), type=int)
    parser.add_argument("-T", "--takes", metavar="TAKE", help="take only the first %(metavar)s elements of each output list default (1 10 5 2)",
        nargs="+", default=( 1, 10, 2, 5 ), type=int)
    parser.add_argument("-E", "--edit-count", metavar="COUNT", help="average self-adjusting benchmarks over %(metavar)s list edits ",
        nargs="+", default=250, type=int)
    parser.add_argument("-S", "--seeds", metavar="SEED", help="run benchmark for seeds (default: 5 random seeds)",
        nargs="+", default=random.sample(xrange(sys.maxint), 5), type=int)
    args = parser.parse_args()
    if args.benchmark is None and args.resummarize is None:
        args.benchmark = "Results/ListOps"
    elif args.resummarize == []:
        args.resummarize = [ "Results/ListOps/latest" ]

    if args.resummarize is not None:
        folder = sorted(args.resummarize)[-1]
        folders = args.resummarize
    else:
        results_dir = time.strftime("%Y-%m-%d-%H-%M-%S")
        folder = os.path.join(args.benchmark, results_dir)
        os.makedirs(folder)
        latest = os.path.join(args.benchmark, "latest")
        try:
            os.unlink(latest)
        except OSError:
            pass
        try:
            os.symlink(results_dir, latest)
        except OSError:
            print>>sys.stderr, traceback.format_exc()
            print>>sys.stderr, "warning: cannot create latest symlink to benchmark"
        folders = [ folder ]

        print>>sys.stderr, "Using seeds %s" % ( args.seeds, )

        pool = multiprocessing.Pool(processes=args.processes, initializer=initrecursionlimit, maxtasksperchild=1)

        for take in args.takes:
            for task in ( list_quicksort_task, list_filter_task, list_map_task ):
                results = []
                try:
                    # don't use pool.apply_async, it's triggers http://bugs.python.org/issue10332
                    for result in pool.imap_unordered(driver, ( ( ListOps, task, take, size, seed, args.edit_count )
                            for size in args.input_sizes
                            for seed in args.seeds
                            for ListOps in random.sample([
                                "EagerListOps", "LazyListOps", "LazyModListOps", "BidirectionalLazyModListOps",
                                "FastBidirectionalLazyModListOps", ], 4) )): #"EagerModListOps",
                        # don't use extend, so that if an exception or interrupt occurs, we still have some results
                        results.append(result)
                except Exception:
                    traceback.print_exc()
                except KeyboardInterrupt:
                    pool.terminate()
                    pool.join()
                    sys.exit()
                finally:
                    with gzip.open(os.path.join(folder, "%s-%02d-%04d.json.gz" % ( task.__name__, take, len(results) )), "w") as jsonfile:
                        json.dump(results, jsonfile, indent=4, separators=( ",", ":" ))


    print>>sys.stderr, "Generating summary in %s ..." % ( folder, )
    files = sorted(set(chain.from_iterable( ( file for file in os.listdir(path) if file.endswith(".json.gz") ) for path in folders )))

    with open(os.path.join(folder, "summary.html"), "w") as htmlfile:
        print>>htmlfile, "<!doctype html>"
        print>>htmlfile, "<meta charset=utf-8>"
        print>>htmlfile, "<style>figure.inline-figure { display: inline-block; margin: 0; }</style>"

        styles = defaultdict(( { "color": color, "linestyle": linestyle, "marker": marker, "markersize": markersize }
            for color, ( linestyle, ( marker, markersize ) ) in izip(
                cycle(( "black", "darkblue", "darkred", "darkgreen", "darkcyan", "dimgray" )),
                cycle(product(( "-", "--", "-." ), ( ( ".", 8 ), ( "^", 6 ), ( "s", 4 ), ( "*", 7 ), ( "D", 4 ) ))) )).next)

        for file in files:
            print>>sys.stderr, "    Summarizing %s ..." % ( file, )
            label = file[:-8]
            summary = os.path.join(folder, label)
            print>>htmlfile, "<h1>%s</h1>" % ( label, )

            try:
                os.makedirs(summary)
            except OSError as e:
                import errno
                if e.errno != errno.EEXIST:
                    raise

            results = []
            for path in folders:
                filepath = os.path.join(path, file)
                print>>sys.stderr, "        Loading %s ..." % ( filepath, ),
                try:
                    results.extend(json.load(gzip.open(filepath), object_pairs_hook=OrderedDict))
                except IOError as e:
                    if e.errno != errno.ENOENT:
                        raise
                    print>>sys.stderr, " not found"
                except Exception:
                    traceback.print_exc()
                else:
                    print>>sys.stderr, " done"

            table = {}
            table["time"] = {}
            table["max-rss"] = {}
            units = {}
            editables = set()
            for record in results:
                try:
                    table["time"].setdefault("from-scratch", {}).setdefault(record["ListOps"], {}) \
                        .setdefault(record["size"], []).append(record["setup-time"] + record["take-time"])
                    table["max-rss"].setdefault("from-scratch", {}).setdefault(record["ListOps"], {}) \
                        .setdefault(record["size"], []).append(record["take-max-rss"])
                    if units and units != record["units"]:
                        raise ValueError("inconsistent units in results:\nexpected: %s\ngot: %s" % ( pprint.pformat(units), pprint.pformat(record["units"]) ))
                    units.update(record["units"])
                except Exception:
                    traceback.print_exc()
                    if "error" in record:
                        pprint.pprint(dict(record))
                else:
                    if "edits" in record:
                        editables.add(record["ListOps"])
                        try:
                            table["time"].setdefault("propagate", {}).setdefault(record["ListOps"], {}) \
                                .setdefault(record["size"], []).append(record["edits"]["total-stats"]["mean"])
                            table["max-rss"].setdefault("propagate", {}).setdefault(record["ListOps"], {}) \
                                .setdefault(record["size"], []).append(record["edits"]["max-rss"])
                            table["time"].setdefault("update", {}).setdefault(record["ListOps"], {}) \
                                .setdefault(record["size"], []).append(record["edits"]["update-stats"]["mean"])
                        except Exception:
                            traceback.print_exc()
                            if "error" in record:
                                pprint.pprint(dict(record))

            from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
            from matplotlib.figure import Figure
            from matplotlib.ticker import FormatStrFormatter
            from scipy import stats

            xmax = {}
            ymax = {}
            xmax["time"] = {}
            ymax["time"] = {}
            for timing, ListOps_table in table["time"].iteritems():
                for ListOps, sizes in ListOps_table.iteritems():
                    for size, values in sizes.iteritems():
                        avg = stats.tmean(values)
                        sizes[size] = avg
                        xmax["time"][timing] = max(xmax["time"].get(timing, 0), size)
                        ymax["time"][timing] = max(ymax["time"].get(timing, 0), avg)
            xmax["max-rss"] = {}
            ymax["max-rss"] = {}
            for memory, ListOps_table in table["max-rss"].iteritems():
                for ListOps, sizes in ListOps_table.iteritems():
                    for size, values in sizes.iteritems():
                        maxrss = max(values)
                        sizes[size] = maxrss
                        xmax["max-rss"][memory] = max(xmax["max-rss"].get(memory, 0), size)
                        ymax["max-rss"][memory] = max(ymax["max-rss"].get(memory, 0), maxrss)

            for measurement, measurement_table in table.iteritems():
                for timing, ListOps_table in measurement_table.iteritems():
                    for ListOps, sizes in ListOps_table.iteritems():
                        ListOps_table[ListOps] = OrderedDict(sorted(sizes.iteritems()))
                    measurement_table[timing] = OrderedDict(
                        sorted(ListOps_table.iteritems(), key=lambda ( ListOps, sizes ): max(sizes.itervalues()), reverse=True))

            scalings = {
                "time": ( ( 1.01, ( "from-scratch", "propagate" ) ), (  5., ( "propagate", ) ) ),
                "max-rss": ( ( 1.01, ( "from-scratch", "propagate" ) ), )
            }

            for measurement, measurement_table in table.iteritems():
                for yadjust, scales in scalings[measurement]:
                    print>>sys.stderr, "        Plotting %s (%.2fx)" % ( measurement, yadjust )
                    svgfilename = "%s-%s-%s-%s.svg" % ( label, measurement, yadjust, "-".join(scales) )
                    with open(os.path.join(summary, svgfilename), "w") as svgfile:
                        fig = FigureCanvas(Figure(figsize=( 3.5, 3 ))).figure
                        ax = fig.add_subplot(1, 1, 1,
                            xlim=( 0, 1.01 * max( xmax[measurement][scale] for scale in scales ) ),
                            ylim=( 0, yadjust * max( ymax[measurement][scale] for scale in scales ) ))

                        ax.set_xlabel("input size", fontsize=8)
                        ax.set_ylabel("%s (%s)" % ( measurement, units[measurement] ), fontsize=8)
                        for axis in ( ax.get_xaxis(), ax.get_yaxis() ):
                            axis.set_major_formatter(FormatStrFormatter("%.3g"))
                            axis.set_ticks_position("none")
                        if hasattr(ax, "tick_params"):
                            ax.tick_params(labelsize=7)
                        for side in ( "left", "bottom" ):
                            ax.spines[side].set_color("silver")
                            ax.spines[side].set_linestyle("dotted")
                            ax.spines[side].set_linewidth(0.5)
                        for side in ( "right", "top" ):
                            ax.spines[side].set_visible(False)
                        ax.grid(linewidth=0.5, linestyle=":", color="silver")

                        for timing in ( "from-scratch", "propagate" ):
                            ListOps_table = measurement_table[timing]
                            for ListOps, sizes in ListOps_table.iteritems():
                                sizes, values = zip(*sizes.iteritems())
                                print>>sys.stderr, "            %24s ... %s" \
                                    % ( "%s (%s)" % ( ListOps, timing ), " ".join( format(value, "9.3g") for value in values ) )
                                ax.plot(sizes, values, label="%s (%s)" % ( ListOps, timing ), markeredgecolor="none", **styles[ListOps, timing])

                        try:
                            ax.legend(loc="best", prop={ "size": 8 }, frameon=False, fancybox=False)
                        except TypeError:
                            ax.legend(loc="best", prop={ "size": 8 }, fancybox=False)

                        if hasattr(fig, "tight_layout"):
                            fig.tight_layout(pad=0.5)

                        fig.savefig(svgfile, format="svg")
                        print>>htmlfile, "<figure class=inline-figure><img src=%s></figure>" \
                            % ( os.path.join(label, svgfilename), )


            svgfilename = "%s-speedup.svg" % ( label, )
            with open(os.path.join(summary, svgfilename), "w") as svgfile:
                fig = FigureCanvas(Figure(figsize=( 3.5, 3 ))).figure
                ax = fig.add_subplot(1, 1, 1,
                    xlim=( 0, 1.01 * xmax["time"]["propagate"] ))
                ax.set_title("Speed-up: X (propagate) / Lazy (from-scratch)", fontsize=8)
                ax.set_xlabel("input size", fontsize=8)
                for axis in ( ax.get_xaxis(), ax.get_yaxis() ):
                    axis.set_major_formatter(FormatStrFormatter("%.3g"))
                    axis.set_ticks_position("none")
                if hasattr(ax, "tick_params"):
                    ax.tick_params(labelsize=7)
                for side in ( "left", "bottom" ):
                    ax.spines[side].set_color("silver")
                    ax.spines[side].set_linestyle("dotted")
                    ax.spines[side].set_linewidth(0.5)
                for side in ( "right", "top" ):
                    ax.spines[side].set_visible(False)
                ax.grid(linewidth=0.5, linestyle=":", color="silver")

                print>>sys.stderr, "        Plotting speed-up ..."
                for ListOps in editables:
                    sizes, speedups = zip(*( ( size, table["time"]["from-scratch"]["Lazy"][size] / value ) \
                        for size, value in table["time"]["propagate"][ListOps].iteritems() ))
                    print>>sys.stderr, "            %24s ... %s" % ( ListOps, " ".join( format(speedup, "9.3g") for speedup in speedups ) )
                    ax.plot(sizes, speedups, label=ListOps, markeredgecolor="none", **styles[ListOps, "propagate"])

                try:
                    ax.legend(loc="best", prop={ "size": 8 }, frameon=False, fancybox=False)
                except TypeError:
                    ax.legend(loc="best", prop={ "size": 8 }, fancybox=False)

                if hasattr(fig, "tight_layout"):
                    fig.tight_layout(pad=0.5)

                fig.savefig(svgfile, format="svg")
                print>>htmlfile, "<figure class=inline-figure><img src=%s></figure>" \
                    % ( os.path.join(label, svgfilename), )


            for ListOps in editables:
                print>>sys.stderr, "        Plotting %s details ..." % ( ListOps, )
                svgfilename = "%s-%s-details.svg" % ( label, ListOps )
                with open(os.path.join(summary, svgfilename), "w") as svgfile:
                    fig = FigureCanvas(Figure(figsize=( 3.5, 3 ))).figure
                    ax = fig.add_subplot(1, 1, 1,
                        xlim=( 0, 1.01 * xmax["time"]["propagate"] ))
                    ax.set_title("%s details" % ( ListOps, ), fontsize=8)
                    ax.set_xlabel("input size", fontsize=8)
                    ax.set_ylabel("time (%s)" % ( units["time"], ), fontsize=8)
                    for axis in ( ax.get_xaxis(), ax.get_yaxis() ):
                        axis.set_major_formatter(FormatStrFormatter("%.3g"))
                        axis.set_ticks_position("none")
                    if hasattr(ax, "tick_params"):
                        ax.tick_params(labelsize=7)
                    for side in ( "left", "bottom" ):
                        ax.spines[side].set_color("silver")
                        ax.spines[side].set_linestyle("dotted")
                        ax.spines[side].set_linewidth(0.5)
                    for side in ( "right", "top" ):
                        ax.spines[side].set_visible(False)
                    ax.grid(linewidth=0.5, linestyle=":", color="silver")

                    for timing in ( "propagate", "update" ):
                        sizes, values = zip(*table["time"][timing][ListOps].iteritems())
                        print>>sys.stderr, "            %24s ... %s" \
                            % ( timing, " ".join( format(value, "9.3g") for value in values ) )
                        ax.plot(sizes, values, label="%s" % ( timing, ), markeredgecolor="none", **styles[ListOps, timing])

                    try:
                        ax.legend(loc="best", prop={ "size": 8 }, frameon=False, fancybox=False)
                    except TypeError:
                        ax.legend(loc="best", prop={ "size": 8 }, fancybox=False)

                    if hasattr(fig, "tight_layout"):
                        fig.tight_layout(pad=0.5)

                    fig.savefig(svgfile, format="svg")
                    print>>htmlfile, "<figure class=inline-figure><img src=%s></figure>" \
                        % ( os.path.join(label, svgfilename), )
