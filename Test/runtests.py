#!/usr/bin/env python
import sys, os

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), os.path.pardir, os.path.pardir, "Source", "Python")))

import MyPyUnit
MyPyUnit.TestProgram()
