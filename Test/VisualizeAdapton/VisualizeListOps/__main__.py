
if __name__ == "__main__":
    import gc, random, os, shutil, sys
    from collections import OrderedDict
    from Adapton.Lazy.Debug import JSONLogger
    from AdaptonTestApps.ListOps import DebugLazyModListOps as ListOps

    random.seed(1)
    w = 10
    xs = random.sample(xrange(w), w)
    kss = ( ( ( True, 2, 4 ), ), )

    def apply_edits(ys, ks):
        for insert, index, value in ks:
            if insert:
                ys = ListOps.insert(ys, index, value)
            else:
                ys = ListOps.delete(ys, index)
        return ys

    output_dir = "ListOps.visual"
    if not os.access(output_dir, os.F_OK):
        os.mkdir(output_dir)

    visualizer_dir = os.path.join(os.path.dirname(__file__), "..", "Visualizer")
    for visualizer_file in os.listdir(visualizer_dir):
        shutil.copy(os.path.join(visualizer_dir, visualizer_file), output_dir)

    with open(os.path.join(output_dir, "toc.json"), "w") as tocjsonfile:
        toc = []
        for label, op in (
                ( "partition-concat", lambda ys: ListOps.concat(*ListOps.partition(4, ys)) ),
                ( "filter", lambda ys: ListOps.filter(lambda y: y % 2, ys) ),
                ( "map", lambda ys: ListOps.map(lambda y: y * 2, ys) ),
                ( "scan", lambda ys: ListOps.scan(lambda y, acc: y + acc, ys, 0) ),
                ( "quicksort", ListOps.quicksort )):

            with open(os.path.join(output_dir, "%s.json" % ( label, )), "w") as jsonfile:
                with JSONLogger(jsonfile) as logger:
                    ys = ListOps.of_list(xs)
                    zs = op(ys)
                    ListOps.to_list(zs)
                    for ks in kss:
                        ys = apply_edits(ys, ks)
                        ListOps.to_list(zs)
                del ys, zs
                gc.collect()
            toc.append(label)

        import json
        json.dump(toc, tocjsonfile, indent=4, separators=( ",", ":" ))