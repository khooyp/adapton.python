/*
Copyright (c) 2012, Michael Bostock
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* The name Michael Bostock may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL MICHAEL BOSTOCK BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
new function() {
    var d3_behavior_zoomInfinity = [0, Infinity];
    d3.behavior.altZoom = function() {
        var translate = [0, 0];
        var scale = 1;
        var scaleExtent = d3_behavior_zoomInfinity;
        var event = d3.dispatch("zoom").zoom;
        var x0, x1, y0, y1;

        function zoom() {
            this
                .on("mousewheel.zoom", mousewheel)
                .on("DOMMouseScroll.zoom", mousewheel);
        }

        zoom.translate = function(x) {
            if (!arguments.length) return translate;
            translate = x.map(Number);
            dispatch(this);
            return zoom;
        };

        zoom.scale = function(x) {
            if (!arguments.length) return scale;
            scale = x;
            dispatch(this);
            return zoom;
        };

        zoom.scaleExtent = function(x) {
            if (!arguments.length) return scaleExtent;
            scaleExtent = x == null ? d3_behavior_zoomInfinity : x.map(Number);
            return zoom;
        };

        zoom.x = function(z) {
            if (!arguments.length) return x1;
            x1 = z;
            x0 = z.copy();
            return zoom;
        };

        zoom.y = function(z) {
            if (!arguments.length) return y1;
            y1 = z;
            y0 = z.copy();
            return zoom;
        };

        function dispatch(target) {
            if (x1) x1.domain(x0.range().map(function(x) { return (x - translate[0]) / scale; }).map(x0.invert));
            if (y1) y1.domain(y0.range().map(function(y) { return (y - translate[1]) / scale; }).map(y0.invert));
            if (d3.event) {
                d3.event.preventDefault();
            }
            try {
                var e0 = d3.event;
                var e1 = {type: "zoom", sourceEvent: e0, target: target, scale: scale, translate: translate};
                d3.event = e1;
                event.apply(zoom);
            } finally {
                d3.event = e0;
            }
        }

        function mousewheel() {
            if (d3.event.altKey) {
                var old_scale = scale;
                var mouse = d3.mouse(this);
                scale = Math.max(scaleExtent[0], Math.min(scaleExtent[1], Math.pow(2, d3.event.wheelDelta * .002) * old_scale));
                translate = [mouse[0] - scale * (mouse[0] - translate[0]) / old_scale, mouse[1] - scale * (mouse[1] - translate[1]) / old_scale];
            } else {
                translate = [translate[0] + d3.event.wheelDeltaX, translate[1] + d3.event.wheelDeltaY];
            }
            dispatch(this);
        }

        return d3.rebind(zoom, event, "on");
    };
}