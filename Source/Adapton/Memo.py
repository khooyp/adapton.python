from functools import partial
from Utilities.Trampoline import *


class Memo(partial):
    __slots__ = ( "_hash", "_signature" )

    def __new__(cls, fn, *args, **kwargs):
        if type(fn) is Memo and not args and not kwargs:
            return fn
        self = super(Memo, cls).__new__(cls, fn, *args, **kwargs)
        # unpack partials/trampolining and build up a tuple of functions, arguments, and keyword arguments
        args = []
        kwargs_list = []
        fn = self
        while True:
            if isinstance(fn, partial):
                args.extend(reversed(fn.args))
                if fn.keywords:
                    kwargs_list.append(fn.keywords)
                fn = fn.func
            elif isinstance(fn, Trampolining):
                fn = fn.fn
            else:
                break
        # merge kwargs and split into list of keywords and list of value
        kwargs = {}
        for kw in reversed(kwargs_list):
            kwargs.update(kw)
        kwargs = zip(*sorted(kwargs.iteritems()))
        # ignore functions with no arguments
        if not args and not kwargs:
            self._hash = None
            return self
        signature = ( fn, tuple(args), tuple(kwargs) )
        try:
            self._hash = hash(signature)
        except:
            self._hash = None
        else:
            self._signature = signature
        return self

    def __hash__(self):
        if self._hash is None:
            raise TypeError
        return self._hash

    def __eq__(self, other):
        return type(other) is Memo \
            and self._hash is not None and other._hash is not None and self._hash == other._hash \
            and self._signature == other._signature
