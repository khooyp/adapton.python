"""
Lazy self-adjusting values.

This version is a non-trampolining version of Bidirectional.
"""

from abc import ABCMeta
from collections import namedtuple
from weakref import WeakKeyDictionary, WeakValueDictionary
from ..Memo import Memo

__all__ = ( "Lazy", "Stats", "measure" )


@apply
def initrecursionlimit():
    import sys, resource
    frame_size = 1024 # rough overestimate
    new_limit = resource.getrlimit(resource.RLIMIT_STACK)[0] // frame_size
    if new_limit < 0:
        new_limit = 2**30 - 1 # 2**31 - 1 leads to some bugs in Python
    if sys.getrecursionlimit() < new_limit:
        sys.setrecursionlimit(new_limit)


class Stats(namedtuple("Stats", ( "create", "memo", "update", "evaluate", "dirty", "clean" ))):
    # create: thunks created
    # memo: thunks memo-hit (not created)
    # update: thunks updated
    # evaluate: thunks (re-)evaluated
    # dirty: thunks to be checked
    # clean: thunks checked clean (not re-evaluated)
    pass

stats_create = 0
stats_memo = 0
stats_update = 0
stats_evaluate = 0
stats_dirty = 0
stats_clean = 0

def measure(fn, *args, **kwargs):
    global stats_create, stats_memo, stats_update, stats_evaluate, stats_dirty, stats_clean
    old_create = stats_create
    old_memo = stats_memo
    old_update = stats_update
    old_evaluate = stats_evaluate
    old_dirty = stats_dirty
    old_clean = stats_clean
    fn(*args, **kwargs)
    return Stats(
        stats_create - old_create,
        stats_memo - old_memo,
        stats_update - old_update,
        stats_evaluate - old_evaluate,
        stats_dirty - old_dirty,
        stats_clean - old_clean
    )


lazy_change_propagation_level = 0
lazy_stack = []
@apply
class lazy_uninitialized(object):
    __slots__ = ()
lazy_memo = WeakValueDictionary()


class Dependency(object):
    __slots__ = ( "node", "value", "dirty" )
    def __init__(self, node, value, dirty=False):
        self.node = node
        self.value = value
        self.dirty = dirty


class Lazy(object):
    __slots__ = ( "thunk", "value", "binders", "dependencies", "dependents", "__weakref__" )

    def __new__(cls, fn, *args, **kwargs):
        global stats_create, stats_memo
        if callable(fn):
            thunk = Memo(fn, *args, **kwargs)
            try:
                if lazy_change_propagation_level:
                    return lazy_memo[thunk]
                else:
                    raise TypeError
            except ( KeyError, TypeError ) as e:
                stats_create += 1
                self = super(Lazy, cls).__new__(cls)
                self.thunk = thunk
                self.value = lazy_uninitialized
                if isinstance(e, KeyError):
                    lazy_memo[thunk] = self
            else:
                stats_memo += 1
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            stats_create += 1
            self = super(Lazy, cls).__new__(cls)
            self.thunk = None
            self.value = fn
        self.binders = []
        self.dependencies = None
        self.dependents = WeakKeyDictionary()
        return self

    def _notify(self, result):
        if self.binders:
            for fn in self.binders:
                fn(result)
            self.binders = []

    def _evaluate(self, **new_kwargs):
        # evaluate and store the results
        # TODO: handle the case when thunk raises an exception
        global stats_evaluate
        lazy_stack.append(self)
        if self.dependencies:
            for dependency in self.dependencies:
                dependency.node.dependents.pop(self, None)
        self.dependencies = []
        self.value = lazy_uninitialized
        mailbox = []
        self.bind(mailbox.append)
        stats_evaluate += 1
        try:
            self._notify(self.thunk(**new_kwargs)) # only the first _notify will take effect (since binders will be emptied then)
        finally:
            assert lazy_stack.pop() is self
        assert mailbox
        self.value = mailbox[0]
        del mailbox

        return self.value

    def _repair(self, **new_kwargs):
        if self.dependencies is None:
            return self._evaluate(**new_kwargs)

        # find and fix inconsistencies via a truncated inorder traversal of dependencies
        global stats_clean
        stack = []
        node = self
        dependencies = list(reversed(self.dependencies))
        while stack or dependencies:
            if dependencies:
                dependency = dependencies[-1]
                if dependency.dirty:
                    dependency.dirty = False
                    stack.append(( node, dependencies ))
                    node = dependency.node
                    if node.dependencies:
                        # node was not updated, and has dependencies (a branch node)
                        dependencies = list(reversed(node.dependencies))
                    else:
                        dependencies = node.dependencies # [] or None
                else:
                    del dependencies[-1]
            else:
                if node.thunk and dependencies is None:
                    # node was updated/unforced (a leaf node)
                    node._evaluate(**new_kwargs)
                    assert node is not self or not stack
                while stack:
                    node, dependencies = stack.pop()
                    dependency = dependencies.pop()
                    if dependency.node.value == dependency.value:
                        stats_clean += 1
                        break
                    # dependency was updated/unforced and its value changed
                    node._evaluate(**new_kwargs)
                    if node is self:
                        assert not stack
                        dependencies = None

        assert self.value is not lazy_uninitialized
        return self.value

    @property
    def is_forced(self):
        return self.dependencies is not None and self.value is not lazy_uninitialized

    def force(self, **new_kwargs):
        if self.thunk:
            global lazy_change_propagation_level
            lazy_change_propagation_level += 1
            try:
                result = self._repair(**new_kwargs)
            finally:
                lazy_change_propagation_level -= 1
        else:
            result = self.value
        try:
            top = lazy_stack[-1]
        except IndexError:
            pass
        else:
            dependency = Dependency(self, result)
            top.dependencies.append(dependency)
            self.dependents[top] = dependency
        return result

    def update(self, fn, *args, **kwargs):
        global stats_update, stats_dirty
        stats_update += 1
        try:
            lazy_memo.pop(self.thunk, None)
        except TypeError:
            pass
        if callable(fn):
            self.thunk = Memo(fn, *args, **kwargs)
            dirty = True
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            dirty = self.value != fn
            self.thunk = None
            self.value = fn
        if self.dependencies:
            for dependency in self.dependencies:
                dependency.node.dependents.pop(self, None)
        if dirty:
            dependents = self.dependents.items()
            while dependents:
                dependent, dependency = dependents.pop()
                if not dependency.dirty:
                    stats_dirty += 1
                    dependency.dirty = True
                    dependents.extend(dependent.dependents.iteritems())
        self.dependencies = None

    def bind(self, fn):
        if self.is_forced:
            fn(self.value)
        else:
            self.binders.append(fn)

    def unbind(self, fn):
        self.binders.remove(fn)

    def yforce(self, **new_kwargs):
        return Ycall(self.force, **new_kwargs)

    def ytailforce(self, **new_kwargs):
        return Ytailcall(self.force, **new_kwargs)

    def __repr__(self):
        if self.is_forced:
            return "Lazy!%r(%r)" % ( self.thunk, self.value )
        elif not self.thunk:
            return "Lazy#%r" % ( self.value, )
        else:
            return "Lazy?%r" % ( self.thunk, )
