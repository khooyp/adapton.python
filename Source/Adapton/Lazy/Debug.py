"""Debug version of lazy self-adjusting values."""

from abc import ABCMeta
from collections import namedtuple, OrderedDict
from weakref import WeakKeyDictionary, WeakValueDictionary
from Utilities.EventRegistry import EventRegistry
from Utilities.Trampoline import *
from ..Memo import Memo

__all__ = (
    "add_handler", "remove_handler",
    "JSONLogger",
    "Lazy"
)


handlers = EventRegistry()
add_handler = handlers.add
remove_handler = handlers.remove


class JSONLogger(object):
    __slots__ = ( "events", "jsonfile" )

    def __init__(self, jsonfile):
        self.jsonfile = jsonfile

    def __enter__(self):
        self.events = []
        add_handler(self.events.append)

    def __exit__(self, exc, val, tb):
        remove_handler(self.events.append)
        if exc is not None:
            from traceback import extract_tb, format_exception_only
            self.events.append(OrderedDict((
                ( "tag", "Exception" ),
                ( "type", "%s.%s" % ( exc.__module__, exc.__name__ ) ),
                ( "value", "".join(format_exception_only(type, val)) ),
                ( "traceback", extract_tb(tb) )
            )))
        import json
        json.dump(self.events, self.jsonfile, indent=4, separators=( ",", ":" ))

    def dispatch(self, value):
        self.events.append(value)


lazy_change_propagation_level = 0
lazy_stack = []
@apply
class lazy_uninitialized(object):
    __slots__ = ()
lazy_memo = WeakValueDictionary()


class Dependency(object):
    __slots__ = ( "node", "value" )
    def __init__(self, node, value):
        self.node = node
        self.value = value


class Lazy(object):
    __slots__ = ( "id", "thunk", "value", "binders", "dependencies", "dependents", "__weakref__" )
    id_counter = 0

    def __new__(cls, fn, *args, **kwargs):
        if callable(fn):
            thunk = Memo(fn, *args, **kwargs)
            try:
                if lazy_change_propagation_level:
                    self = lazy_memo[thunk]
                else:
                    raise TypeError
            except ( KeyError, TypeError ) as e:
                self = super(Lazy, cls).__new__(cls)
                self.id = Lazy.id_counter
                Lazy.id_counter += 1
                self.thunk = thunk
                self.value = lazy_uninitialized
                if isinstance(e, KeyError):
                    lazy_memo[thunk] = self
                handlers.dispatch(OrderedDict(( ( "tag", "AddNode" ), ( "id", self.id ) )))
                handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDefer" ), ( "id", self.id ) )))
            else:
                handlers.dispatch(OrderedDict(( ( "tag", "MemoNode" ), ( "id", self.id ) )))
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self = super(Lazy, cls).__new__(cls)
            self.id = Lazy.id_counter
            Lazy.id_counter += 1
            self.thunk = None
            self.value = fn
            handlers.dispatch(OrderedDict(( ( "tag", "AddNode" ), ( "id", self.id ) )))
        self.binders = []
        self.dependencies = None
        self.dependents = WeakKeyDictionary()
        return self

    def __del__(self):
        if handlers is None: # program has terminated
            return
        if self.dependencies:
            for target in self.dependencies:
                handlers.dispatch(OrderedDict(( ( "tag", "RemoveEdge" ), ( "source", self.id ), ( "target", target.node.id ) )))
        elif self.thunk and self.dependencies is None:
            handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDone" ), ( "id", self.id ) )))
        handlers.dispatch(OrderedDict(( ( "tag", "RemoveNode"), ( "id", self.id ) )))

    def _notify(self, result):
        if self.binders:
            for fn in self.binders:
                fn(result)
            self.binders = []

    @Trampolining
    def _evaluate(self, **new_kwargs):
        # evaluate and store the results
        # TODO: handle the case when thunk raises an exception
        handlers.dispatch(OrderedDict(( ( "tag", "EvaluateNodeStart" ), ( "id", self.id ) )))
        if self.dependencies:
            for target in self.dependencies:
                target.node.dependents.pop(self, None)
                handlers.dispatch(OrderedDict(( ( "tag", "RemoveEdge" ), ( "source", self.id ), ( "target", target.node.id ) )))
        lazy_stack.append(self)
        self.dependencies = []
        self.value = lazy_uninitialized
        mailbox = []
        self.bind(mailbox.append)
        try:
            self._notify((yield Ycall(self.thunk, **new_kwargs))) # only the first _notify will take effect (since binders will be emptied then)
        finally:
            assert lazy_stack.pop() is self
        handlers.dispatch(OrderedDict(( ( "tag", "EvaluateNodeStop" ), ( "id", self.id ) )))
        assert mailbox
        self.value = mailbox[0]
        del mailbox

        yield self.value

    @Trampolining
    def _repair(self, **new_kwargs):
        if self.dependencies is None:
            yield Ycall(self._evaluate, **new_kwargs)
            handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDone" ), ( "id", self.id ) )))
            yield self.value

        # find and fix inconsistencies via a truncated inorder traversal of dependencies
        visited = set()
        stack = []
        node = self
        dependencies = list(reversed(self.dependencies))
        while stack or dependencies:
            if dependencies:
                handlers.dispatch(OrderedDict(( ( "tag", "WalkEdgeForward" ), ( "source", node.id ), ( "target", dependencies[-1].node.id ) )))
                stack.append(( node, dependencies ))
                node = dependencies[-1].node
                if node in visited:
                    dependencies = []
                else:
                    visited.add(node)
                    if node.dependencies:
                        # node was not updated, and has dependencies (a branch node)
                        dependencies = list(reversed(node.dependencies))
                    else:
                        dependencies = node.dependencies # [] or None
            else:
                if node.thunk and dependencies is None:
                    # node was updated/unforced (a leaf node)
                    yield Ycall(node._evaluate, **new_kwargs)
                    handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDone" ), ( "id", node.id ) )))
                    assert node is not self or not stack
                while stack:
                    node, dependencies = stack.pop()
                    dependency = dependencies.pop()
                    handlers.dispatch(OrderedDict(( ( "tag", "WalkEdgeBackward" ), ( "source", node.id ), ( "target", dependency.node.id ) )))
                    if dependency.node.value == dependency.value:
                        break
                    # dependency was updated/unforced and its value changed
                    yield Ycall(node._evaluate, **new_kwargs)
                    if node is self:
                        assert not stack
                        dependencies = None

        assert self.value is not lazy_uninitialized
        yield self.value

    @property
    def is_forced(self):
        return self.dependencies is not None and self.value is not lazy_uninitialized

    @Trampolining
    def force(self, **new_kwargs):
        global lazy_change_propagation_level
        if self.thunk:
            if not lazy_change_propagation_level:
                handlers.dispatch(OrderedDict(( ( "tag", "ChangePropagationStart" ), ( "id", self.id ) )))
            lazy_change_propagation_level += 1
            try:
                top = lazy_stack[-1]
            except IndexError:
                pass
            else:
                handlers.dispatch(OrderedDict(( ( "tag", "AddEdge" ), ( "source", top.id ), ( "target", self.id ) )))
            try:
                result = yield Ycall(self._repair, **new_kwargs)
            finally:
                lazy_change_propagation_level -= 1
            if not lazy_change_propagation_level:
                handlers.dispatch(OrderedDict(( ( "tag", "ChangePropagationStop" ), ( "id", self.id ) )))
        else:
            try:
                top = lazy_stack[-1]
            except IndexError:
                pass
            else:
                handlers.dispatch(OrderedDict(( ( "tag", "AddEdge" ), ( "source", top.id ), ( "target", self.id ) )))
            result = self.value
        try:
            top = lazy_stack[-1]
        except IndexError:
            pass
        else:
            top.dependencies.append(Dependency(self, result))
            self.dependents[top] = result
        yield result

    def update(self, fn, *args, **kwargs):
        if self.dependencies:
            for target in self.dependencies:
                target.node.dependents.pop(self, None)
                handlers.dispatch(OrderedDict(( ( "tag", "RemoveEdge" ), ( "source", self.id ), ( "target", target.node.id ) )))
        elif self.thunk and self.dependencies is None:
            handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDone" ), ( "id", self.id ) )))
        handlers.dispatch(OrderedDict((( "tag", "UpdateNodeDefer" ), ( "id", self.id ) )))
        try:
            lazy_memo.pop(self.thunk, None)
        except TypeError:
            pass
        if callable(fn):
            self.thunk = Memo(fn, *args, **kwargs)
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self.thunk = None
            self.value = fn
        self.dependencies = None

    def bind(self, fn):
        if self.is_forced:
            fn(self.value)
        else:
            self.binders.append(fn)

    def unbind(self, fn):
        self.binders.remove(fn)

    def yforce(self, **new_kwargs):
        return Ycall(self.force, **new_kwargs)

    def ytailforce(self, **new_kwargs):
        return Ytailcall(self.force, **new_kwargs)

    def __repr__(self):
        if self.is_forced:
            return "Lazy!%r(%r)" % ( self.thunk, self.value )
        elif not self.thunk:
            return "Lazy#%r" % ( self.value, )
        else:
            return "Lazy?%r" % ( self.thunk, )
