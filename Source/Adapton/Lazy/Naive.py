"""
Lazy self-adjusting values.

This version uses a naive change propagation strategy based on a generalized inorder traversal of the dependency graph.
"""

from abc import ABCMeta
from weakref import WeakValueDictionary
from Utilities.Trampoline import *
from ..Memo import Memo

__all__ = ( "Lazy", )


lazy_change_propagation_level = 0
lazy_stack = []
@apply
class lazy_uninitialized(object):
    __slots__ = ()
lazy_memo = WeakValueDictionary()


class Dependency(object):
    __slots__ = ( "node", "value" )
    def __init__(self, node, value):
        self.node = node
        self.value = value


class Lazy(object):
    __slots__ = ( "thunk", "value", "binders", "dependencies", "__weakref__" )

    def __new__(cls, fn, *args, **kwargs):
        if callable(fn):
            thunk = Memo(fn, *args, **kwargs)
            try:
                if lazy_change_propagation_level:
                    return lazy_memo[thunk]
                else:
                    raise TypeError
            except ( KeyError, TypeError ) as e:
                self = super(Lazy, cls).__new__(cls)
                self.thunk = thunk
                self.value = lazy_uninitialized
                if isinstance(e, KeyError):
                    lazy_memo[thunk] = self
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self = super(Lazy, cls).__new__(cls)
            self.thunk = None
            self.value = fn
        self.binders = []
        self.dependencies = None
        return self

    def _notify(self, result):
        if self.binders:
            for fn in self.binders:
                fn(result)
            self.binders = []

    @Trampolining
    def _evaluate(self, **new_kwargs):
        # evaluate and store the results
        # TODO: handle the case when thunk raises an exception
        lazy_stack.append(self)
        self.dependencies = []
        self.value = lazy_uninitialized
        mailbox = []
        self.bind(mailbox.append)
        try:
            self._notify((yield Ycall(self.thunk, **new_kwargs))) # only the first _notify will take effect (since binders will be emptied then)
        finally:
            assert lazy_stack.pop() is self
        assert mailbox
        self.value = mailbox[0]
        del mailbox

        yield self.value

    @Trampolining
    def _repair(self, **new_kwargs):
        if self.dependencies is None:
            yield Ytailcall(self._evaluate, **new_kwargs)

        # find and fix inconsistencies via a truncated inorder traversal of dependencies
        visited = set()
        stack = []
        node = self
        dependencies = list(reversed(self.dependencies))
        while stack or dependencies:
            if dependencies:
                stack.append(( node, dependencies ))
                node = dependencies[-1].node
                if node in visited:
                    dependencies = []
                else:
                    visited.add(node)
                    if node.dependencies:
                        # node was not updated, and has dependencies (a branch node)
                        dependencies = list(reversed(node.dependencies))
                    else:
                        dependencies = node.dependencies # [] or None
            else:
                if node.thunk and dependencies is None:
                    # node was updated/unforced (a leaf node)
                    yield Ycall(node._evaluate, **new_kwargs)
                    assert node is not self or not stack
                while stack:
                    node, dependencies = stack.pop()
                    dependency = dependencies.pop()
                    if dependency.node.value == dependency.value:
                        break
                    # dependency was updated/unforced and its value changed
                    yield Ycall(node._evaluate, **new_kwargs)
                    if node is self:
                        assert not stack
                        dependencies = None

        assert self.value is not lazy_uninitialized
        yield self.value

    @property
    def is_forced(self):
        return self.dependencies is not None and self.value is not lazy_uninitialized

    @Trampolining
    def force(self, **new_kwargs):
        if self.thunk:
            global lazy_change_propagation_level
            lazy_change_propagation_level += 1
            try:
                result = yield Ycall(self._repair, **new_kwargs)
            finally:
                lazy_change_propagation_level -= 1
        else:
            result = self.value
        try:
            top = lazy_stack[-1]
        except IndexError:
            pass
        else:
            top.dependencies.append(Dependency(self, result))
        yield result

    def update(self, fn, *args, **kwargs):
        try:
            lazy_memo.pop(self.thunk, None)
        except TypeError:
            pass
        if callable(fn):
            self.thunk = Memo(fn, *args, **kwargs)
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self.thunk = None
            self.value = fn
        self.dependencies = None

    def bind(self, fn):
        if self.is_forced:
            fn(self.value)
        else:
            self.binders.append(fn)

    def unbind(self, fn):
        self.binders.remove(fn)

    def yforce(self, **new_kwargs):
        return Ycall(self.force, **new_kwargs)

    def ytailforce(self, **new_kwargs):
        return Ytailcall(self.force, **new_kwargs)

    def __repr__(self):
        if self.is_forced:
            return "Lazy!%r(%r)" % ( self.thunk, self.value )
        elif not self.thunk:
            return "Lazy#%r" % ( self.value, )
        else:
            return "Lazy?%r" % ( self.thunk, )
