"""Eager self-adjusting values."""

import heapq
from abc import ABCMeta, abstractproperty
from sys import maxint
from weakref import ref, WeakSet, WeakValueDictionary
from Utilities.Trampoline import *
from .Memo import Memo

__all__ = ( "Mod", )


TO_T = 1.4
TO_LABEL_BITS = maxint.bit_length()
TO_MAX_LABEL = 1 << (TO_LABEL_BITS - 1)
TO_GAP_SIZE = TO_MAX_LABEL / TO_LABEL_BITS
TO_END_LABEL = TO_MAX_LABEL - TO_GAP_SIZE


class TotalOrder(object):
    class Parent(object):
        __slots__ = ( "prev", "next", "children", "label" )

        def __init__(self, children):
            self.prev = self
            self.next = self
            self.children = children
            self.label = 0

        def add(self, children):
            cls = self.__class__
            new = cls.__new__(cls)
            new.prev = self
            new.next = self.next
            new.children = children
            new.next.prev = new
            self.next = new

            if new.next.label > self.label:
                label = new.next.label
            else:
                label = self.label + 2

            if self.label + 1 == label:
                new.label = label
                new.rebalance()
            else:
                new.label = (self.label + label) >> 1

            return new

        def __lt__(self, other):
            if type(other) is self.__class__:
                return self.label < other.label
            return NotImplemented

        def __le__(self, other):
            if type(other) is self.__class__:
                return self.label <= other.label
            return NotImplemented

        def rebalance(self):
            upper = self
            count = 1
            mask = 1
            tau = 1. / TO_T

            while True:
                lo_label = self.label & ~mask
                hi_label = self.label | mask

                while self.prev.label >= lo_label and self.prev.label <= self.label:
                    self = self.prev
                    count += 1

                while upper.next.label <= hi_label and upper.next.label >= upper.label:
                    upper = upper.next
                    count += 1

                density = count / float(mask + 1)
                if density < tau:
                    break

                mask = (mask << 1) | 1
                tau /= TO_T

            incr = (mask + 1) / count
            while True:
                self.label = lo_label
                if self is upper:
                    break
                self = self.next
                lo_label += incr

    __slots__ = ( "parent", "next", "label", "reference" )

    def __init__(self, *args, **kwargs):
        raise NotImplementedError("call %s.make to construct %s" % ( self.__class__.__name__, self.__class__.__name__ ))

    @classmethod
    def make(cls):
        class TotalOrder(cls):
            __slots__ = ()
            make = property()
        self = TotalOrder.__new__(TotalOrder)
        self.parent = TotalOrder.Parent(self)
        self.next = None
        self.label = 0
        self.reference = None
        return self

    def add(self, reference=None):
        if self.removed():
            raise ValueError("%s.add cannot add to removed self" % ( self.__class__.__name__, ))
        new = self.__class__.__new__(self.__class__)
        new.parent = self.parent
        if self.next is None:
            new.next = None
            new.label = (self.label + TO_MAX_LABEL) >> 1
        else:
            new.next = self.next
            new.label = (self.label + self.next.label) >> 1
        self.next = new
        if reference is not None:
            new.reference = ref(reference)
        else:
            new.reference = None
        if self.label == new.label:
            self.rebalance()
        return new

    def __lt__(self, other):
        if type(other) is self.__class__:
            return self.parent < other.parent or self.parent == other.parent and self.label < other.label
        return NotImplemented

    def __le__(self, other):
        if type(other) is self.__class__:
            return self.parent < other.parent or self.parent == other.parent and self.label <= other.label
        return NotImplemented

    def splice(self, other):
        if not type(other) == self.__class__:
            raise TypeError("%s.splice other must be derived from the same total order as self" % ( self.__class__.__name__, ))
        if self.removed():
            raise ValueError("%s.splice cannot splice removed self" % ( self.__class__.__name__, ))
        if other.removed():
            raise ValueError("%s.splice cannot splice removed other" % ( self.__class__.__name__, ))
        if self > other:
            raise ValueError("%s.splice other must be greater than or equal to self\n%s\n%s\n" % ( self.__class__.__name__, self, other ))
        if self is other:
            return

        parent = self.parent
        next = self.next
        while True:
            if next is None:
                parent = parent.next
                next = parent.children
            elif self < next and next < other:
                current = next
                next = next.next
                current.label = None
                current.next = self
            else:
                break

        if self.parent is other.parent:
            self.next = other
        else:
            self.next = None
            self.parent.next = other.parent
            other.parent.prev = self.parent
            other.parent.children = other

    def removed(self):
        return self.label is None

    def rebalance(self):
        parent = self.parent
        current = self.parent.children
        while current is not None:
            num = 0
            while num < TO_END_LABEL and current is not None:
                if current.reference is not None and current.reference() is None:
                    if parent.children is current:
                        parent.children = current.next
                    else:
                        prev.next = current.next
                else:
                    prev = current
                    current.parent = parent
                    current.label = num
                    num += TO_GAP_SIZE
                current = current.next

            if current is not None:
                prev.next = None
                parent = parent.add(current)

    def __repr__(self):
        return "TotalOrder(%r)" % ( self.label, )


eager_stack = []
eager_queue = []
eager_memo = WeakValueDictionary()
eager_now = TotalOrder.make()
eager_finger = eager_now


def insert_time(parent=None):
    global eager_now
    eager_now = eager_now.add(parent)
    return eager_now


@Trampolining
def propagate_changes():
    global eager_finger, eager_now
    while True:
        try:
            mod = heapq.heappop(eager_queue)
        except IndexError:
            return
        if not mod.start_timestamp.removed():
            last_finger = eager_finger
            eager_now = mod.start_timestamp
            eager_finger = mod.end_timestamp
            yield Ycall(mod._evaluate)
            eager_finger = last_finger
            eager_now.splice(mod.end_timestamp)


class Mod(object):
    __slots__ = ( "thunk", "value", "dependencies", "dependents", "start_timestamp", "end_timestamp", "__weakref__" )

    def __init__(self, *args, **kwargs):
        raise NotImplementedError("call %s.make to construct %s" % ( self.__class__.__name__, self.__class__.__name__ ))

    def __lt__(self, other):
        if isinstance(other, Mod):
            return self.start_timestamp < other.start_timestamp
        return NotImplemented

    @Trampolining
    @classmethod
    def make(cls, fn, *args, **kwargs):
        if callable(fn):
            thunk = Memo(fn, *args, **kwargs)
            try:
                if eager_stack:
                    global eager_now
                    mod = eager_memo[thunk]
                    if mod.start_timestamp.removed() or mod.start_timestamp <= eager_now or mod.end_timestamp >= eager_finger:
                        raise KeyError
                    eager_now.splice(mod.start_timestamp)
                    eager_now = mod.end_timestamp
                    yield mod
                else:
                    raise TypeError
            except ( KeyError, TypeError ) as e:
                self = cls.__new__(cls)
                self.value = None
                self.thunk = thunk
                self.dependencies = set()
                self.dependents = WeakSet()
                self.start_timestamp = insert_time(self)
                yield Ycall(self._evaluate)
                self.end_timestamp = insert_time(self)
                if isinstance(e, KeyError):
                    eager_memo[thunk] = self
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(fn))
            self = cls.__new__(cls)
            self.value = fn
            self.thunk = None
            self.dependencies = set()
            self.dependents = WeakSet()
            self.start_timestamp = insert_time(self)
            self.end_timestamp = insert_time(self)
        yield self


    @Trampolining
    def _evaluate(self):
        self.dependencies.clear()
        eager_stack.append(self)
        try:
            value = yield Ycall(self.thunk)
        finally:
            assert eager_stack.pop() is self
        self._update(value)

    def _update(self, value):
        if self.value == value:
            return
        self.value = value
        for dependent in self.dependents:
            if not dependent.start_timestamp.removed():
                heapq.heappush(eager_queue, dependent)
        self.dependents.clear()

    def force(self):
        try:
            top = eager_stack[-1]
        except IndexError:
            pass
        else:
            top.dependencies.add(self)
            self.dependents.add(top)
        return self.value

    refresh = staticmethod(propagate_changes)

    def update(self, fn, *args, **kwargs):
        try:
            eager_memo.pop(self.thunk, None)
        except TypeError:
            pass
        if callable(fn):
            self.thunk = Memo(fn, *args, **kwargs)
            self._evaluate()
        else:
            self.thunk = None
            self._update(value)

    def yforce(self):
        return Ycall(self.force)

    def ytailforce(self):
        return Ytailcall(self.force)

    def __repr__(self):
        return "Eager(%r,%r,%r)" % ( self.value, self.start_timestamp, self.end_timestamp )
