
__all__ = ( "tfold", )


def tfold(f, xs):
    if not xs:
        raise ValueError("tfold xs must have at least one element")
    while len(xs) > 1:
        i = 0
        ys = xs
        xs = []
        while i < len(ys):
            xs.append(ys[i] if i == len(ys) - 1 else f(ys[i], ys[i + 1]))
            i += 2
    return xs[0]
