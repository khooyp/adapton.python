
__all__ = ( "EventRegistry", )

class EventRegistry(object):
    __slots__ = ( "handlers", )

    def __init__(self):
        self.handlers = []

    def add(self, fn):
        self.handlers.append(fn)

    def remove(self, fn, *args, **kwargs):
        self.handlers.remove(fn)

    def dispatch(self, *args, **kwargs):
        for fn in self.handlers:
            fn(*args, **kwargs)
