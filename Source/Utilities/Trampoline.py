"""Trampolining decorator for deep recursion and tail calls."""

import functools, sys

__all__ = ( "Ycall", "Ytailcall", "Trampolining" )


# note that this module avoids using isinstance for type tests, since they occur quite frequently, particularly in
# code written in typical recursive style, so much that they show up when profiled


class Ycall(functools.partial):
    """Wrap a function to be called by the caller's trampoline."""
    __slots__ = ()
    def __new__(cls, fn, *args, **kwargs):
        if type(fn) is Trampolining:
            return fn.ycall(*args, **kwargs)
        else:
            return super(Ycall, cls).__new__(cls, fn, *args, **kwargs)


class Ytailcall(functools.partial):
    """Wrap a function to be tail-called by the caller's trampoline (discarding the caller's frame)."""
    __slots__ = ()
    def __new__(cls, fn, *args, **kwargs):
        if type(fn) is Trampolining:
            return fn.ytailcall(*args, **kwargs)
        else:
            return super(Ytailcall, cls).__new__(cls, fn, *args, **kwargs)


class Trampolining(object):
    """
    Trampolining decorator.

    The decorated function may use yield in the following manner:

        # call a function f without using the call stack (to support deep recursion)
        z = yield Ycall(f)
        z = yield Ycall(f, x, y=y)
        yield Ycall(f)

        # call a function f and immediately return its return value (i.e., make a tail call)
        yield Ytailcall(f)
        yield Ytailcall(f, x, y=y)

        # return a value
        yield x # return the value of x to the called (where x is not Ycall or Ytailcall)

    Alternatively, the decorated function may use return instead of yield, in which case Ycall is treated as Ytailcall.
    """

    __slots__ = ( "fn", )

    def __init__(self, fn):
        super(Trampolining, self).__setattr__("fn", fn)

    def __get__(self, obj, objtype=None):
        return self.__class__(self.fn.__get__(obj, objtype))

    def __call__(self, *args, **kwargs):
        from types import GeneratorType
        fn = self.fn(*args, **kwargs)

        # trampoline
        stack = []
        throw = None
        while True:
            try:
                while type(fn) in ( Ycall, Ytailcall ):
                    # handle tail-call via return
                    fn = fn()
            except:
                if not stack:
                    raise
                throw = sys.exc_info()
                fn = stack.pop()
            else:
                if type(fn) is GeneratorType:
                    value = None
                else:
                    if not stack:
                        return fn
                    value = fn
                    fn = stack.pop()

            # handle generator
            while True:
                try:
                    if throw:
                        try:
                            value = fn.throw(*throw)
                        finally:
                            throw = None
                    else:
                        value = fn.send(value)
                except StopIteration:
                    value = None
                except:
                    if not stack:
                        raise
                    throw = sys.exc_info()
                else:
                    if type(value) is Ycall:
                        stack.append(fn)
                        fn = value
                        break
                    if type(value) is Ytailcall:
                        # handle tail-call via yield
                        fn = value
                        break

                if not stack:
                    return value
                fn = stack.pop()

    def ycall(self, *args, **kwargs):
        """Wrap this function with Ycall, to be called by the caller's trampoline."""
        return Ycall(self.fn, *args, **kwargs)

    def ytailcall(self, *args, **kwargs):
        """Wrap this function with Ytailcall, to be tail-called by the caller's trampoline (discarding the caller's frame)."""
        return Ytailcall(self.fn, *args, **kwargs)

    # forward some special attributes to self.fn
    def __metaclass__(name, bases, dict):
        class forwarding(object):
            def __init__(self, attr):
                self.attr = attr
            def __get__(self, obj, objtype=None):
                if obj is None:
                    return self
                return getattr(obj.fn, self.attr)
            def __set__(self, obj, value):
                setattr(obj.fn, self.attr, value)
            def __delete__(self, obj):
                delattr(obj.fn, self.attr)
        for name in ( "__module__", "__name__", "__doc__", "__dict__", "__str__", "__format__" ):
            dict[name] = forwarding(name)
        return type(name, bases, dict)

    # wrap __repr__
    def __repr__(self):
        return "Trampolining(%r)" % ( self.fn, )

    # forward other attributes to self.fn
    def __getattr__(self, attr):
        return getattr(self.fn, attr)
    def __setattr__(self, attr, value):
        setattr(self.fn, attr, value)
    def __delattr__(self, attr):
        delattr(self.fn, attr)
