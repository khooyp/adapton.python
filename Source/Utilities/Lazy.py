"""Lazy values."""

from abc import ABCMeta, abstractmethod
from contextlib import contextmanager
from .Trampoline import *

__all__ = (
    "enable_repr_peek", "peeking",
    "Lazyable", "Lazy", "LazyVal",
)


enable_repr_peek = False

@contextmanager
def peeking():
    global enable_repr_peek
    was_peeking_enabled = peeking.enabled
    was_repr_peek_enabled = enable_repr_peek
    peeking.enabled = True
    enable_repr_peek = True
    yield
    peeking.enabled = was_peeking_enabled
    enable_repr_peek = was_repr_peek_enabled

peeking.enabled = False


class Lazyable(object):
    __metaclass__ = ABCMeta
    __slots__ = ()

    def force(self, **new_kwargs):
        return self

    def yforce(self, **new_kwargs):
        return Ycall(self.force, **new_kwargs)

    def ytailforce(self, **new_kwargs):
        return Ytailcall(self.force, **new_kwargs)

    is_forced = True

    def maybe_force(self):
        if self.is_forced:
            return self.force()
        else:
            return self

    def peek(self):
        with peeking():
            return self.force()


class Lazy(Lazyable):
    __slots__ = ( "thunk", "binders", "is_forced" )

    def __new__(cls, thunk, *args, **kwargs):
        if callable(thunk):
            self = super(Lazy, cls).__new__(cls)
            self.thunk = Ycall(thunk, *args, **kwargs)
            self.binders = []
            self.is_forced = False
        else:
            if args or kwargs:
                raise TypeError("'%s' object is not callable" % type(thunk))
            self = LazyVal(thunk)
        return self


    def _notify(self, result):
        assert not self.is_forced
        if self.binders:
            for fn in self.binders:
                fn(result)
            if not peeking.enabled:
                self.binders = []

    @Trampolining
    def force(self, **new_kwargs):
        if not self.is_forced:
            # evaluate and store the results
            # TODO: handle the case when thunk raises an exception
            mailbox = []
            self.bind(mailbox.append)
            self._notify((yield Ycall(self.thunk, **new_kwargs))) # only the first _notify will take effect (since binders will be emptied then)
            assert mailbox
            result = mailbox[0]
            if peeking.enabled:
                yield result
            else:
                self.thunk = result
                self.is_forced = True
        yield self.thunk

    def bind(self, fn):
        if self.is_forced:
            fn(self.thunk)
        else:
            self.binders.append(fn)

    def __repr__(self):
        if self.is_forced:
            return "Lazy!(%r)" % ( self.force(), )
        elif enable_repr_peek:
            return "Lazy(%r)" % ( self.peek(), )
        else:
            return "Lazy?%r" % ( self.thunk, )


class LazyVal(Lazyable):
    __slots__ = ( "value", )

    def __new__(cls, value):
        # provide a singleton for LazyVal(None), to make "x is LazyVal(None)" be consistent with "x is None"
        if value is None:
            return LazyNone
        self = super(LazyVal, cls).__new__(cls)
        self.value = value
        return self

    def force(self, **new_kwargs):
        return self.value

    def __repr__(self):
        return "LazyVal(%r)" % ( self.value, )


@apply
class LazyNone(LazyVal):
    __slots__ = ()

    def __new__(cls):
        return super(LazyVal, cls).__new__(cls) # skip LazyVal's __new__

    def force(self, **new_kwargs):
        return None

    def __repr__(self):
        return "LazyVal(None)"
