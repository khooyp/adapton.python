Introduction
============

Adapton.python is an Python library for self-adjusting computation.


Quick-start
===========

1. Run Adapton.python unit tests:

        % make test

2. Start an Python REPL with Adapton.python in PYTHONPATH:

        % make repl
        Python 2.7.6 (default, Nov 18 2013, 15:12:51)
        [GCC 4.2.1 Compatible Apple LLVM 5.0 (clang-500.2.79)] on darwin
        Type "help", "copyright", "credits" or "license" for more information.
        >>> from Adapton.Lazy import *
        >>> x = Lazy(1)
        >>> y = Lazy(2)
        >>> z = Lazy(lambda: x.force() + y.force())
        >>> z.force()
        3
        >>> x.update(5)
        >>> z.force()
        7
        >>> y.update(4)
        >>> z.force()
        9

3. Generate a visualization of Adapton.python in operation:

        % make visualize-listops
        # open ListOps.visual/visualize.html in Safari (doesn't work in other browsers)
